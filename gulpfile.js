const gulp = require('gulp');
const babel = require('gulp-babel');
const del = require('del');

gulp.task('build', () =>
  gulp.src('./src/**/*')
    .pipe(babel())
    .pipe(gulp.dest('dist'))
);

gulp.task('clean', () =>
  del(['dist/*'])
);
