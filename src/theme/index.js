import { Dimensions } from 'react-native'

export const COLOR_ACCENT = '#FFC107'
export const COLOR_ACCENT_LIGHT = '#FFF176'
export const COLOR_DANGER = '#F44336'
export const COLOR_DIVISOR = '#757575'
export const COLOR_DIVISOR_LIGHT = '#D1D8DC'
export const COLOR_INPUT = '#f3f3f3'
export const COLOR_LIGHT = '#FFFFFF'
export const COLOR_PRIMARY = '#616161'
export const COLOR_PRIMARY_DARK = '#424242'
export const COLOR_TEXT_PRIMARY = '#A0A5A8'
export const COLOR_TEXT_SECONDARY = '#607D8B'
export const COLOR_BG = '#FFFFFF'

export const DIMENSION_INPUT_WIDTH = 300
export const DIMENSION_SCREEN_WIDTH = Dimensions.get('screen').width
export const DIMENSION_SCREEN_HEIGHT = Dimensions.get('screen').height
export const DIMENSION_TEXT_PRIMARY = 28
export const DIMENSION_TEXT_SECONDARY = 18
export const DIMENSION_TEXT_AVATAR = 38
export const DIMENSION_ICONE_BARRA_SUPERIROR = 30
export const DIMENSION_TEXTO_BARRA_SUPERIROR = 22
