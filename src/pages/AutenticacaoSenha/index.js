import React from 'react';
import { View, Animated, Easing } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  BotaoCircular,
  BotaoVoltar,
  Painel,
  Container,
  Texto,
  InputSecundario
} from '../../components';

import * as usuarioActions from '../../actions/usuario';
import style from './style';

class AutenticacaoSenha extends React.Component {

  constructor(props) {
    super(props)
    this.spinValue = new Animated.Value(0)
  }

  componentDidMount() {
    this.spin()
  }

  spin() {
    this.spinValue.setValue(0)
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear
      }
    ).start(() => this.spin())
  }

  render() {
    const { usuarioActions } = this.props.actions;
    const { usuarioReducer } = this.props.reducers;
    const { navigation } = this.props

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    return (
      <Container>
        <BotaoVoltar
          onPress={() => navigation.goBack()}
        />

        <Painel>
          <View style={style.tituloView}>
            <Texto estilo={style.titulo} font="malgun">Olá</Texto>
            <Texto estilo={style.nome} font="malgun"> {usuarioReducer.nome}!</Texto>
          </View>

          <InputSecundario
            placeholder="Insira sua senha"
            secureTextEntry
            value={usuarioReducer.senha}
            onChangeText={usuarioActions.handleSenha}
          />

          {usuarioReducer.loading ?
            <Animated.View style={{ transform: [{ rotate: spin }] }}>
              <BotaoCircular
                icone="md-refresh"
                disabled={true}
              />
            </Animated.View>
            :
            <BotaoCircular
              icone="md-checkmark"
              onPress={() => {
                if (usuarioReducer.senha) {
                  usuarioActions.autenticar(usuarioReducer.nome, navigation)
                }
              }}
            />
          }
        </Painel>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer
  }
});

const mapDispatchToProps = (dispatch) => ({
  actions: {
    usuarioActions: bindActionCreators(usuarioActions, dispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AutenticacaoSenha);
