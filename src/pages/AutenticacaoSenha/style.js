import { StyleSheet } from 'react-native'

import {
  COLOR_ACCENT
} from '../../theme'

const style = StyleSheet.create({
  tituloView: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  nome: {
    top: 33,
    fontSize: 20
  },
  titulo: {
    color: COLOR_ACCENT,
    fontSize: 55,
    alignSelf: 'center'
  }
});

export default style;
