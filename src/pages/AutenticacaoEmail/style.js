import { StyleSheet } from 'react-native'

import {
  COLOR_ACCENT
} from '../../theme'

const style = StyleSheet.create({
  titulo: {
    color: COLOR_ACCENT,
    fontSize: 55,
    alignSelf: 'center'
  }
})

export default style
