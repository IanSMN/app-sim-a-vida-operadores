import React from 'react';
import { View, Animated, Easing, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SocketIO from 'socket.io-client'

import {
  Container,
  Painel,
  Texto,
  InputSecundario,
  BotaoCircular,
  Botao
} from '../../components';
import Env from '../../environment/desenv'

import * as usuarioActions from '../../actions/usuario';
import style from './style';

class AutenticacaoEmail extends React.Component {

  constructor(props) {
    super(props)
    // this.socket = SocketIO(Env.SOCKET_API)
    this.spinValue = new Animated.Value(0)
  }

  componentDidMount() {
    this.spin()
  }

  componentWillMount() {
    AsyncStorage.clear()
  }

  spin() {
    this.spinValue.setValue(0)
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear
      }
    ).start(() => this.spin())
  }

  render() {
    const { usuarioActions } = this.props.actions;
    const { usuarioReducer } = this.props.reducers;
    const { navigation } = this.props

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    return (
      <Container>
        <Painel>
          <Texto estilo={style.titulo} font="malgun">Olá</Texto>

          <InputSecundario
            value={usuarioReducer.nome}
            onChangeText={usuarioActions.handleNome}
            placeholder="Insira seu nome"
          />

          {usuarioReducer.loading ?
            <Animated.View style={{ transform: [{ rotate: spin }] }}>
              <BotaoCircular
                icone="md-refresh"
                disabled={true}
              />
            </Animated.View>
            :
            <BotaoCircular
              icone="md-checkmark"
              onPress={() => {
                if (usuarioReducer.nome) {
                  usuarioActions.autenticarUsuario(usuarioReducer.nome, navigation)
                }
              }}
            />
          }
        </Painel>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer
  }
});
const mapDispatchToProps = (dispatch) => ({
  actions: {
    usuarioActions: bindActionCreators(usuarioActions, dispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AutenticacaoEmail);
