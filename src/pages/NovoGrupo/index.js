import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { CheckList, BarraSuperior } from '../../components';
import { bindActionCreators } from 'redux'
import * as tabActions from '../../actions/tab'

class NovoGrupo extends React.Component {

  constructor(props) {
    super(props)
    this.socket = this.props.navigation.state.params.socket
  }

  render() {
    const { solicitantesReducer, usuarioReducer } = this.props.reducers;

    return (
      <View style={{ flex: 1, paddingTop: 75 }}>
        <BarraSuperior
          estilo={{ elevation: 0 }}
          buttonLeft={true}
          titulo="Novo grupo"
          subtitulo="Selecionar operadores"
          iconLeft="md-arrow-back"
          onPressButtonLeft={() => {
            this.props.navigation.goBack();
          }}
        />

        <CheckList
          handleTab={this.props.actions.tabActions.handleTab}
          navigation={this.props.navigation}
          lista={solicitantesReducer.operadores}
          usuario={usuarioReducer}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    solicitantesReducer: state.solicitantesReducer,
    usuarioReducer: state.usuarioReducer
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    tabActions: bindActionCreators(tabActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(NovoGrupo);


