import React from 'react'
import { View, Text } from 'react-native'
import { Bubble } from 'react-native-gifted-chat'
import style from './style'

export default props => (
  <View
  // style={{flexDirection: 'row'}}
  >
    {/* <View style={{width:36, height:36, borderRadius: 50, backgroundColor: 'red'}}></View> */}
    <Text style={style.nome}>{props.currentMessage.user.name}</Text>
    <Bubble
      {...props}
      onLongPress={() => null}
      wrapperStyle={{
        left: {
          backgroundColor: '#FFE9A7',
          borderBottomRightRadius: 7,
          borderBottomLeftRadius: 0,
          borderTopRightRadius: 7,
          borderTopLeftRadius: 7,
          paddingHorizontal: 7,
          marginBottom: 3,
          elevation: 2,
        },
        right: {
          borderBottomRightRadius: 0,
          backgroundColor: '#fdfdfd',
          borderBottomLeftRadius: 7,
          borderTopRightRadius: 7,
          borderTopLeftRadius: 7,
          paddingHorizontal: 7,
          marginBottom: 3,
          elevation: 2,
        }
      }}
      textStyle={{
        right: { color: '#424242' },
        left: { color: '#4E4D4D' },
      }}
    >
    </Bubble>
  </View>
)
