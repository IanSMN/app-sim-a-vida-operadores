import { StyleSheet } from 'react-native';
import { DIMENSION_SCREEN_WIDTH, DIMENSION_SCREEN_HEIGHT } from '../../theme'

const style = StyleSheet.create({
  sendView: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    marginHorizontal: 10,
    marginBottom: 4

  },
  iconSend: {
    fontSize: 25,
    color: '#607d8b',
  },
  scale: {
    width: DIMENSION_SCREEN_WIDTH,
    height: DIMENSION_SCREEN_HEIGHT,
  },
  nome: {
    fontSize: 13,
    color: '#607d8b',
    marginBottom: 2,
    marginTop: 3,
    marginLeft: 3
  }
})

export default style;
