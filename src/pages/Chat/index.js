// ADD LINE IN GIFTTEDCHAT
// import {ptBr} from 'moment/locale/pt-br'

import React from 'react'
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  Keyboard,
  BackHandler,
  Linking,
  ActivityIndicator,
  findNodeHandle,
  AsyncStorage
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import EmojiPanel from 'react-native-emoji-panel'
import { GiftedChat, Send, LoadEarlier } from 'react-native-gifted-chat'
import { BarraSuperior, Icone, Alerta } from '../../components'
import * as solicitantesActions from '../../actions/solicitantes'
import * as conversandoActions from '../../actions/conversando'
import BubbleComponent from './bubble'
import style from './style'
import TextInputReset from 'react-native-text-input-reset'

class Chat extends React.Component {

  constructor(props) {
    super(props)
    this.socket = this.props.navigation.state.params.socket
    this.usuarioReducer = this.props.reducers.usuarioReducer
    this.solicitantesReducer = this.props.reducers.solicitantesReducer
    this.solicitantesActions = this.props.actions.solicitantesActions
    this.conversandoActions = this.props.actions.conversandoActions
    this.params = this.props.navigation.state.params
    this.messageBox
    this.tipo
    this.sala
    this.nome = '',
      this.imagem = '',
      this.state = {
        inputText: '',
        oldMessages: false,
        emoji: false,
        userId: this.usuarioReducer.id,
        imagem: null,
        loadMore: false,
        pagina: 1,
        modalConfirmacao: false,
        autenticado: false,
        carregado: false
      }
  }

  componentDidMount() {
    console.log(this.props.navigation)
    this.tipo = this.params.tipo

    this.socket.on('chatEmAtendimento', () => {
      Alert.alert(
        'Atenção!',
        'Este usuário acabou de entrar em um atendimento'
      )
      this.props.navigation.goBack()
    })

    // this.socket.on('autenticacao', () => {
    //   console.warn('AUTENTICADO')
    //   this.setState({ autenticado: true })
    // })

    this.conversandoActions.handleTipo(this.tipo)
    this.conversandoActions.handleConversando(true)

    if (this.tipo == 1) {
      this.socket.emit('iniciarChat', {
        linhas: 10,
        paginas: 1,
        tipoConversa: 1,
        destinatarios: [this.params.item.id],
        idRemetente: this.usuarioReducer.id
      })

      this.nome = this.params.item.apelido || this.params.item.nome
      this.imagem = this.params.item.imagem
    }

    if (this.tipo == 2) {
      this.socket.emit('iniciarChat', {
        linhas: 10,
        paginas: 1,
        tipoConversa: 2,
        sala: this.params.item.id,
        idRemetente: this.usuarioReducer.id
      })

      this.nome = this.params.item.apelido || this.params.item.nome
      this.imagem = this.params.item.imagem
    }

    if (this.tipo == 3) {
      if (!this.params.item.continuando) {
        this.socket.emit('iniciarChatUser', {
          sala: this.params.item.sala,
          op: this.usuarioReducer.id
        })
      }

      AsyncStorage.setItem('usuario', this.params.item.user.nome)

      this.nome = this.params.item.user.nome
      this.imagem = this.params.item.user.imagem
    }

    this.socket.on('sala', sala => {

      this.sala = sala.sala

      if (this.params.notificacao) {
        this.socket.emit('msgVisualizada', {
          sala: this.sala,
          user: this.usuarioReducer.id
        })
      }

      let messages = this.props.reducers.conversandoReducer.dadosMensagem;

      if (this.state.carregado) {
        this.setState({ carregado: false })
        if (sala.historico) {

          // console.log(sala.historico)

          if (sala.historico.length) {

            let oldMessages = []
            let _id
            let historico = []

            historico = sala.historico.reverse()

            // historico.reverse().map((item, index) => {
            historico.map((item, index) => {
              let message
              let nome

              this.state.loadMore ? _id = messages.length + (index + 1) : _id = index + 1

              if (this.tipo == 2) {
                this.solicitantesReducer.operadores.map((value) => {
                  if (value.id == item.idRemetente) {
                    nome = value.nome
                  }
                })
              }

              if (item.idRemetente === this.usuarioReducer.id) {
                message = {
                  _id: _id,
                  text: item.mensagem,
                  createdAt: item.dataEnvio,
                  user: {
                    name: nome || ''
                  }
                }
              } else {
                message = {
                  _id: _id,
                  text: item.mensagem,
                  createdAt: item.dataEnvio,
                  user: {
                    _id: this.usuarioReducer.id,
                    name: nome
                  }
                }
              }

              !this.state.loadMore ? messages = messages.concat(message) : oldMessages = oldMessages.concat(message)
            })

            this.conversandoActions.handleCont(_id)

            if (!this.state.loadMore) {
              this.conversandoActions.renderMessage(messages, false)
              console.log(oldMessages)
            }
            else {
              const messagesUpdate = [...oldMessages]
              this.conversandoActions.renderMessage(messagesUpdate, true)
              console.log(messagesUpdate)
            }

            if (this.props.reducers.conversandoReducer.dadosMensagem.length < sala.historico[0].totalLinhas) {
              this.setState({ ...this.state, oldMessages: true })
            }
            else {
              this.setState({ ...this.state, oldMessages: false })
            }
          }
        }

        this.conversandoActions.handleSala(this.sala)

      } else { }
    })

    this.socket.on('desistirDoAtendimento', desistiu => {
      if (!this.props.reducers.conversandoReducer.conversando &&
        this.props.reducers.conversandoReducer.tipo === 3) {
        return
      }
      this.props.navigation.goBack()
      Alert.alert(
        'Atenção!',
        'Usuário desistiu da chamada'
      )
      AsyncStorage.removeItem('usuario')
    })

    Keyboard.addListener('keyboardDidShow', this.onKeyBoardShow)
    BackHandler.addEventListener('hardwareBackPress', this.onKeyBoardShow)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onKeyBoardShow)
    Keyboard.removeAllListeners('keyboardDidShow')
    this.conversandoActions.handleConversando(false)
    this.conversandoActions.handleMensagem([])
    this.conversandoActions.handleTipo(1)
  }

  componentWillMount() {
    this.setState({ carregado: true })
  }

  onKeyBoardShow = () => {
    this.setState({ ...this.state, emoji: false })
    return true
  }

  onSend = (messages) => {
    let sala
    this.tipo == 3 ? sala = this.params.item.sala : sala = this.sala
    this.conversandoActions.renderMessage(messages.reverse(), false)

    this.socket.emit('mensagem', {
      tipoConversa: this.tipo,
      idRemetente: this.usuarioReducer.id,
      idDestinatario: this.params.item.id,
      sala: sala,
      mensagem: this.state.inputText
    })

    TextInputReset.resetKeyboardInput(findNodeHandle(this.messageBox.textInput))
  }

  carregarAnteriores = () => {
    this.setState({
      ...this.state,
      loadMore: true,
      pagina: ++this.state.pagina
    })

    this.socket.emit('historicoMensagens', {
      linhas: 10,
      paginas: this.state.pagina,
      tipoConversa: this.tipo,
      sala: this.sala,
    })
  }

  renderSend(props) {
    return (
      <Send {...props}>
        <View style={style.sendView}>
          <Icone style={style.iconSend} icone="md-send" />
        </View>
      </Send>
    )
  }

  inputTextChange(text) {
    this.setState({ ...this.state, inputText: text })
  }

  renderEmojis = () => {
    this.state.emoji ? this.messageBox.textInput.focus() : Keyboard.dismiss()
    this.setState({ ...this.state, emoji: !this.state.emoji })
  }

  handlePick = (emoji) => {
    const { inputText } = this.state
    this.setState({ inputText: inputText + emoji })
  }

  deletarGrupo(navigation) {
    this.socket.emit('excluirGrupo', {
      idGrupo: this.params.item.id,
      idUser: this.props.reducers.usuarioReducer.id
    })

    const grupos = this.solicitantesReducer.grupos.filter(grupos => grupos.id !== this.params.item.id)
    this.solicitantesActions.handleGrupos(grupos)
    navigation.goBack()
  }

  renderActions = () => {
    return (
      <TouchableOpacity
        onPress={this.renderEmojis}
        style={{
          alignSelf: 'center',
          paddingLeft: 15
        }}>
        {!this.state.emoji ?
          <Icone style={{ fontSize: 23, color: '#868686' }} icone="md-happy" /> :
          <Icone style={{ fontSize: 23, color: '#868686' }} lib="font" icone="keyboard-o" />
        }
      </TouchableOpacity>
    )
  }

  render(props) {
    // console.log('PROPS DADOS MENSAGEM:', this.props.reducers.conversandoReducer.dadosMensagem)
    const { navigation } = this.props
    const { index } = this.params
    const criador = (this.tipo == 2 && this.params.item.administrador == true) ? true : false
    const mensagemOld = this.props.reducers.conversandoReducer.dadosMensagemOld.slice() || []

    return (
      <View style={{ flex: 1, backgroundColor: '#eaeaea' }} >
        <Image
          style={[style.scale, {
            position: 'absolute',
            opacity: 0.3
          }]}
          source={require('../../assets/bgChat.jpg')}
        />
        <BarraSuperior
          estilo={{ position: 'relative', marginBottom: 10 }}
          titulo={this.nome || 'Anônimo'}
          imagem={{ uri: this.imagem || null }}
          notificacao={this.props.reducers.notificacoesReducer.operadores.length}
          iconRightBefore={'headset'}
          buttonRight={this.tipo == 3 || criador == true ? true : false}
          buttonRightBefore={this.tipo == 3 || criador == true ? true : false}
          iconRight={this.tipo == 3 ? 'more-vert' : 'delete'}
          iconLeft={'md-arrow-back'}
          buttonLeft={this.tipo == 3 ? false : true}
          onPressButtonRight={() => {
            this.tipo == 3 ? navigation.navigate({
              key: 'perfil',
              routeName: 'Perfil',
              params: { ...this.params.item, index }
            })
              :
              this.setState({ ...this.state, modalConfirmacao: true })
          }}
          onPressButtonLeft={() => {
            this.conversandoActions.handleConversando(false)
            this.conversandoActions.handleMensagem([])
            navigation.goBack()
          }}
          onPressbuttonRightBefore={() => {
            this.conversandoActions.handleMensagemOld(
              mensagemOld.concat(
                this.props.reducers.conversandoReducer.dadosMensagem
              ).reverse()
            )
            this.conversandoActions.handleConversando(false)
            this.conversandoActions.handleMensagem([])
            this.conversandoActions.handleTipo(1)
            navigation.navigate({
              key: 'operadores',
              routeName: 'Operadores',
              params: { tipo: 1, item: this.params.item, index, socket: this.socket }
            })
          }}
        />

        <GiftedChat
          loadEarlier={true}
          renderLoadEarlier={(props) => {
            if (this.state.oldMessages) {
              return (
                <LoadEarlier
                  {...props}
                  label="Carregar anteriores"
                />
              )
            }
          }}
          parsePatterns={(linkStyle) => [
            {
              pattern: /(((https?:\/\/)|(www\.))[^\s]+)/g,
              style: { color: '#0095ff' },
              onPress: props => Linking.openURL(props)
            },
          ]}
          onLoadEarlier={this.carregarAnteriores}
          renderLoading={() => <ActivityIndicator size="large" />}
          ref={ref => this.messageBox = ref}
          focusTextInput={this.renderActions}
          text={this.state.inputText}
          messages={this.props.reducers.conversandoReducer.dadosMensagem}
          placeholder="Digite alguma coisa..."
          locale="pt-br"
          renderAvatar={null}
          minInputToolbarHeight={45}
          renderActions={this.renderActions}
          renderSend={(props) => this.renderSend(props)}
          renderDay={() => null}
          onInputTextChanged={(text) => this.inputTextChange(text)}
          onSend={(messages) => this.onSend(messages)}
          textInputProps={{ lineHeight: 22, fontSize: 'Roboto', fontSize: 18, paddingBottom: 7 }}
          // renderBubble={() => (<BubbleComponent />)}
          renderBubble={BubbleComponent}
        />
        {this.state.emoji &&
          <EmojiPanel
            bgColor="#f5f5f5"
            showSwitchMenu={true}
            onPick={this.handlePick}
          />
        }

        {/* <Modal
          visible={!this.state.autenticado}
          transparent={true}
          onRequestClose={() => this.setState({ autenticado: false })}
          animationType={'fade'}
        >
          <View style={estilo.wrapper}>
            <Text style={estilo.text}>Carregando...</Text>
          </View>
        </Modal> */}

        <Alerta
          visivel={this.state.modalConfirmacao}
          onDelete={() => this.deletarGrupo(navigation)}
          onHide={() => this.setState({ ...this.state, modalConfirmacao: false })}
        />
      </View>
    )
  }
}

// const estilo = StyleSheet.create({
//   wrapper: {
//     position: 'absolute',
//     width: '100%',
//     height: '100%',
//     top: 0,
//     left: 0,
//     backgroundColor: 'rgba(0, 0, 0, 0.6)',
//     justifyContent: 'center',
//     alignItems: 'center',
//     zIndex: 9
//   },
//   text: {
//     color: '#fff',
//     fontSize: 25
//   }
// })

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer,
    solicitantesReducer: state.solicitantesReducer,
    notificacoesReducer: state.notificacoesReducer,
    conversandoReducer: state.conversandoReducer
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    solicitantesActions: bindActionCreators(solicitantesActions, dispatch),
    conversandoActions: bindActionCreators(conversandoActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
