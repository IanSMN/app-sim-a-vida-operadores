import React from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NavigationActions } from 'react-navigation'
import { ListaSimples, BarraSuperior } from '../../components'
import * as conversandoActions from '../../actions/conversando'
import * as notificacoesActions from '../../actions/notificacoes'

const Operadores = ({ reducers, actions, navigation, tipo, socket }) => {
  const { solicitantesReducer } = reducers
  const { notificacoesActions } = actions
  let notificacao = false
  let params = null
  let index = null

  if (navigation.state && navigation.state.params) {
    params = navigation.state.params.item
    index = navigation.state.params.index
  }
  console.log(navigation)

  return (
    <View style={{ flex: 1 }}>
      {params &&
        <BarraSuperior
          buttonLeft={true}
          titulo="Operadores"
          iconLeft="md-arrow-back"
          onPressButtonLeft={() => {
            // actions.conversandoActions.handleConversando(true)
            // actions.conversandoActions.handleSala(params.sala)
            // actions.conversandoActions.handleMensagemOld([])
            navigation.goBack()
            // actions.conversandoActions.handleMensagem(reducers.conversandoReducer.dadosMensagemOld.reverse())
            // navigation.dispatch(NavigationActions.back())
            // navigation.dispatch(
            //   NavigationActions.navigate({
            //     key: 'chat',
            //     routeName: 'Chat',
            //     params: {
            //       item: {
            //         ...params,
            //         continuando: true
            //       },
            //       index,
            //       tipo: tipo || navigation.state.params.tipo,
            //       socketD
            //     }
            //   }
            // ))
          }}
        />
      }
      <View style={params && { marginTop: 65 }}>
        <ListaSimples
          onPressChat={(item, index) => {
            if (item.notificacoes > 0) {
              notificacoesActions.removerNotificacoes(index, 'operadores', 'idRemetente')
              notificacao = true
            }
            navigation.navigate({
              key: 'chatOperador',
              routeName: 'Chat',
              params: {
                item,
                index,
                tipo: tipo || navigation.state.params.tipo,
                notificacao,
                socket: socket || navigation.state.params.socket
              }
            })
          }}
          lista={solicitantesReducer.operadores}
        />
      </View>
    </View >
  )
}

const mapStateToProps = (state) => ({
  reducers: {
    solicitantesReducer: state.solicitantesReducer,
    conversandoReducer: state.conversandoReducer,
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    notificacoesActions: bindActionCreators(notificacoesActions, dispatch),
    conversandoActions: bindActionCreators(conversandoActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Operadores)
