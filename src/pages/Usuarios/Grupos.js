import React from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ListaSimples, BotaoAction } from '../../components'

import * as notificacoesActions from '../../actions/notificacoes'

const Grupos = ({ reducers, navigation, tipo, actions, retorno, socket }) => {
  const { solicitantesReducer } = reducers
  const { notificacoesActions } = actions
  let notificacao = false

  return (
    <View style={{ flex: 1, paddingBottom: 10 }}>
      <ListaSimples
        onPressChat={(item, index) => {
          if (item.notificacoes > 0) {
            notificacoesActions.removerNotificacoes(index, 'grupos', 'idChat')
            notificacao = true
          }
          navigation.navigate({
            key: 'grupos',
            routeName: 'Chat',
            params: {
              item,
              index,
              tipo,
              notificacao,
              retorno,
              socket
            }
          })
        }}
        lista={solicitantesReducer.grupos}
      />
      <BotaoAction
        estiloIcon={{ color: '#ffb300' }}
        estilo={{ backgroundColor: '#FFF' }}
        icone="md-add"
        onPress={(item, index) =>
          navigation.navigate({ routeName: 'NovoGrupo', params: { item, index, socket } })
        }
      />
    </View>
  )
}

const mapStateToProps = (state) => ({
  reducers: {
    solicitantesReducer: state.solicitantesReducer,
    usuarioReducer: state.usuarioReducer
  }
})
const mapDispatchToProps = (dispatch) => ({
  actions: {
    notificacoesActions: bindActionCreators(notificacoesActions, dispatch),
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Grupos)
