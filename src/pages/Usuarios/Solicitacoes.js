import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Lista, Texto } from '../../components';
import * as solicitantesActions from '../../actions/solicitantes';

const Solicitacoes = ({ reducers, navigation, tipo, retorno, socket }) => {
  const { solicitantesReducer } = reducers;

  return (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      {solicitantesReducer.solicitantes.length ?
        <Lista
          onPressChat={(item, index) => {
            navigation.navigate({
              key: 'chatUsuario',
              routeName: 'Chat',
              params: { item, index, tipo, retorno, socket }
            })
          }}
          status={true}
          lista={solicitantesReducer.solicitantes}
        />
        :
        <Texto estilo={{ alignSelf: 'center' }}>
          Nenhum solicitante encontrado...
        </Texto>
      }
    </View>
  );
}

const mapStateToProps = (state) => ({
  reducers: {
    solicitantesReducer: state.solicitantesReducer
  }
});

const mapDispatchToProps = (dispatch) => ({
  actions: {
    solicitantesActions: bindActionCreators(solicitantesActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Solicitacoes);
