import React from 'react'

import { TabViewAnimated } from 'react-native-tab-view'

import {
  TabBar,
  Icone,
  Texto,
  BotaoAction,
  ModalResumoDia
} from '../../components'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Operadores from './Operadores'
import Grupos from './Grupos'
import Solicitacoes from './Solicitacoes'
import * as usuarioActions from '../../actions/usuario'
import * as notificacoesActions from '../../actions/notificacoes'
import * as solicitantesActions from '../../actions/solicitantes'
import * as conversandoActions from '../../actions/conversando'
import * as tabActions from '../../actions/tab'
import { View, Alert, AsyncStorage } from 'react-native'

class Usuarios extends React.Component {
  constructor(props) {
    super(props)
    this.socket = this.props.screenProps.socket
    this.inicio = ''
    this.tipoConversa = 2

    this.state = {
      modalResumo: false,
    }

    this.solicitantesActions = this.props.actions.solicitantesActions
    this.notificacoesActions = this.props.actions.notificacoesActions
    this.tabActions = this.props.actions.tabActions
  }

  componentDidMount() {
    setTimeout(() => {
      this.socket.emit('autenticacao', {
        token: this.props.reducers.usuarioReducer.token,
        tipo: 'operador'
      })
    }, 2000)

    this.socket.on('notificacoes', notificacoes => {
      if (notificacoes.notificacoesParticulares.length) {
        this.notificacoesActions.notificarOperadores(notificacoes.notificacoesParticulares)
      }
      if (notificacoes.notificacoesGrupos.length) {
        this.notificacoesActions.notificarGrupos(notificacoes.notificacoesGrupos)
      }
    })

    this.socket.on('users', (operadores) => {
      // console.warn(JSON.stringify(operadores.content))
      const listaOperadores = operadores.content
      const notificacoes = this.props.reducers.notificacoesReducer.operadores

      listaOperadores.map((item, index) => {
        const notificacao = notificacoes.find((notificacao) => notificacao.idRemetente === listaOperadores[index].id)
        if (notificacao) {
          listaOperadores[index].notificacoes = notificacao ? notificacao.mensagens : 0
        }
      })

      this.solicitantesActions.listarOperadores(listaOperadores)
    })

    this.socket.on('grupos', grupos => {
      const listaGrupos = grupos
      const notificacoes = this.props.reducers.notificacoesReducer.grupos

      listaGrupos.map((item, index) => {
        const notificacao = notificacoes.find((notificacao) => listaGrupos[index].id === notificacao.idChat)
        listaGrupos[index].notificacoes = notificacao ? notificacao.mensagens : 0
      })
      this.solicitantesActions.listarGrupos(listaGrupos)
    })

    this.socket.on('atualizarGrupos', grupos => {
      this.socket.emit('grupos', {
        idUser: this.props.reducers.usuarioReducer.id,
      });
    })

    this.socket.on('conversasPendentes', usuarios => {
      this.solicitantesActions.listarSolicitantes(usuarios)
    })

    this.socket.on('retorno', dadosMensagem => {
      this.socket.emit('msgEntregue', {
        idMsg: dadosMensagem.idMsg,
        idDestinatario: this.props.reducers.usuarioReducer.id,
      })

      this.retornoMensagem(dadosMensagem)
    })
  }

  componentWillUnmount() {
    this.finalizarSessao()
  }

  retornoMensagem = (dadosMensagem) => {
    const sala = this.props.reducers.conversandoReducer.sala
    const mensagemOld = this.props.reducers.conversandoReducer.dadosMensagemOld

    if (dadosMensagem.tipoConversa === 1 && this.props.reducers.conversandoReducer.tipoConversa === 3) {
      const operadores = this.props.reducers.solicitantesReducer.operadores
      const operadoresNotificados = this.notificacao(operadores, dadosMensagem, 'operadores')
      this.solicitantesActions.handleOperadores(operadoresNotificados)
    }

    if (dadosMensagem.tipoConversa === 3 && mensagemOld.length) {
      let id = this.props.reducers.conversandoReducer.dadosMensagem.length + 1
      const message = {
        _id: id,
        text: dadosMensagem.mensagem,
        createdAt: Date.now(),
        user: {
          _id: this.props.reducers.usuarioReducer.id
        }
      }

      this.props.actions.conversandoActions.handleMensagemOld(mensagemOld.concat(message))
      return
    }

    if (this.props.reducers.conversandoReducer.conversando == false || dadosMensagem.sala !== sala) {
      if (dadosMensagem.idRemetente !== this.props.reducers.usuarioReducer.id) {
        if (dadosMensagem.tipoConversa === 2) {
          const grupos = this.props.reducers.solicitantesReducer.grupos
          const gruposNotificados = this.notificacao(grupos, dadosMensagem, 'grupos')
          this.solicitantesActions.listarGrupos(gruposNotificados)
        }

        if (dadosMensagem.tipoConversa === 1) {
          const operadores = this.props.reducers.solicitantesReducer.operadores
          const operadoresNotificados = this.notificacao(operadores, dadosMensagem, 'operadores')
          this.solicitantesActions.handleOperadores(operadoresNotificados)
        }
      }
    }

    else {
      if (dadosMensagem.idRemetente !== this.props.reducers.usuarioReducer.id) {
        if (dadosMensagem.sala === sala) {

          let cont = this.props.reducers.conversandoReducer.dadosMensagem.length + 1
          const tipo = this.props.reducers.conversandoReducer.tipo
          let nome


          this.socket.emit('msgVisualizada', {
            sala: sala,
            user: this.props.reducers.usuarioReducer.id
          })

          if (tipo == 2) {
            this.props.reducers.solicitantesReducer.operadores.map((item, index) => {
              if (item.id == dadosMensagem.idRemetente) {
                nome = item.nome
              }
            })
          }

          const message = {
            _id: cont,
            text: dadosMensagem.mensagem,
            createdAt: Date.now(),
            user: {
              _id: this.props.reducers.usuarioReducer.id,
              name: nome || ''
            }
          }

          this.props.actions.conversandoActions.renderMessage(message, false)
        }
      }
    }
  }

  notificacao = (tipoSolicitantes, usuario, tipo) => {
    const tipoSolicitante = tipoSolicitantes
    let filtro

    if (tipo == 'operadores') filtro = (solicitante) => solicitante.id === usuario.idRemetente
    if (tipo == 'grupos') filtro = (solicitante) => solicitante.id === usuario.sala

    const remetente = tipoSolicitante.find(filtro)
    const notificacaoReducer = this.props.reducers.notificacoesReducer[tipo]

    let notificante
    if (tipo == 'operadores') {
      notificante = notificacaoReducer.find((notificante) => notificante.idRemetente === usuario.idRemetente)
    }
    else {
      notificante = notificacaoReducer.find((notificante) => notificante.idChat === usuario.sala)
    }
    const dataNotificacao = {
      idChat: usuario.sala,
      mensagens: 1
    }
    if (tipo == 'operadores') {
      dataNotificacao.idRemetente = remetente.id
    }
    if (!notificacaoReducer.length) {
      const notificado = [dataNotificacao]
      if (tipo == 'operadores') this.notificacoesActions.notificarOperadores(notificado)
      if (tipo == 'grupos') this.notificacoesActions.notificarGrupos(notificado)
    }
    else {
      notificante ? notificante.mensagens = ++notificante.mensagens : notificacaoReducer.push(dataNotificacao)
      if (tipo == 'operadores') this.notificacoesActions.notificarOperadores(notificacaoReducer)
      if (tipo == 'grupos') this.notificacoesActions.notificarGrupos(notificacaoReducer)
    }
    remetente.notificacoes = remetente.notificacoes > 0 ? ++remetente.notificacoes : 1
    return [...tipoSolicitante]
  }

  onSessao = () => {
    if (!this.props.reducers.notificacoesReducer.sessao) {
      this.socket.emit('inicioDoDia')
      this.notificacoesActions.handleSessao(true)
    }
    else {
      this.setState({
        ...this.state,
        modalResumo: true
      })
    }
  }

  finalizarSessao = () => {
    if (this.props.reducers.usuarioReducer.descricao != '') {
      this.solicitantesActions.listarSolicitantes([])

      this.setState({
        ...this.state,
        modalResumo: false,
      })

      this.notificacoesActions.handleSessao(false)

      this.socket.emit('resumoDoDia', {
        descricao: this.props.reducers.usuarioReducer.descricao
      })
    }
  }

  cenas = ({ route }) => {
    const { navigation } = this.props
    switch (route.key) {
      case '2':
        return <Operadores tipo={1} navigation={navigation} socket={this.socket} />
      case '3':
        return <Grupos tipo={2} navigation={navigation} socket={this.socket} />
      default:
        return (
          <View style={{ flex: 1, justifyContent: 'center' }}>
            {this.props.reducers.notificacoesReducer.sessao ?
              <Solicitacoes
                tipo={3}
                navigation={navigation}
                retorno={this.retornoMensagem}
                socket={this.socket}
              />
              :
              <Texto estilo={{ alignSelf: 'center', alignItems: 'center' }}>
                Para iniciar a sessão, aperte o botão abaixo
              </Texto>
            }
            <BotaoAction
              estiloIcon={
                this.props.reducers.notificacoesReducer.sessao == true ?
                  { color: '#f44336' } :
                  { color: '#4caf50' }
              }
              estilo={{ backgroundColor: '#FFF' }}
              lib="material"
              icone="power-settings-new"
              onPress={this.onSessao}
            />
            <ModalResumoDia
              visivel={this.state.modalResumo}
              onPress={() => {
                this.finalizarSessao()
                this.props.actions.usuarioActions.handleDescricao('')
              }}
              onHide={() => {
                this.props.actions.usuarioActions.handleDescricao('')
                this.setState({
                  ...this.state, modalResumo: false
                })
              }}
            />
          </View>
        )
    }
  }

  deslogarOperador = () => {
    this.props.actions.usuarioActions.deslogar()
    this.socket.disconnect()
    AsyncStorage.clear()
  }

  header = props => {
    const { navigation, actions } = this.props
    return <TabBar
      {...props}
      renderIcon={(props) => this.icone(props)}
      titulo={this.props.reducers.usuarioReducer.nome}
      onPressButton={() => this.deslogarOperador()}
    />
  }

  icone = (props) => {
    const { index } = props
    return (
      <View {...props} style={{ position: 'absolute', bottom: 7 }}>
        {this.iconeNotificacao(index)}
      </View>
    )
  }

  iconeNotificacao = (index) => {
    if (index == 1 && this.props.reducers.notificacoesReducer.operadores.length) {
      return <Icone style={{ color: '#fff', fontSize: 8 }} lib="materialCommunity" icone="circle" />
    }
    if (index == 2 && this.props.reducers.notificacoesReducer.grupos.length) {
      return <Icone style={{ color: '#fff', fontSize: 8 }} lib="materialCommunity" icone="circle" />
    }
  }

  onChange = index => this.tabActions.handleTab(index)

  render() {
    return (
      <TabViewAnimated
        navigationState={this.props.reducers.tabReducer}
        renderScene={this.cenas}
        renderHeader={this.header}
        onIndexChange={this.onChange}
      />
    )
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    tabReducer: state.tabReducer,
    usuarioReducer: state.usuarioReducer,
    notificacoesReducer: state.notificacoesReducer,
    solicitantesReducer: state.solicitantesReducer,
    conversandoReducer: state.conversandoReducer
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    usuarioActions: bindActionCreators(usuarioActions, dispatch),
    notificacoesActions: bindActionCreators(notificacoesActions, dispatch),
    solicitantesActions: bindActionCreators(solicitantesActions, dispatch),
    conversandoActions: bindActionCreators(conversandoActions, dispatch),
    tabActions: bindActionCreators(tabActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Usuarios)
