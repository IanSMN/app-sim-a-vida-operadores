import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  tituloView: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  titulo: {
    marginBottom: 20,
    fontSize: 20
  }
});

export default style;
