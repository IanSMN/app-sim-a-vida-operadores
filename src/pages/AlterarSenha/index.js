import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  BotaoCircular,
  BotaoVoltar,
  Painel,
  Container,
  Texto,
  InputPrimario
} from '../../components';

import * as alterarSenhaActions from '../../actions/alterarSenha';

import style from './style';

const AlterarSenha = ({ actions, reducers, navigation }) => {
  const { alterarSenhaReducer } = reducers;
  const { alterarSenhaActions } = actions;

  return (
    <Container>
      <BotaoVoltar
        onPress={() => navigation.goBack()}
      />

      <Painel>
        <View style={style.tituloView}>
          <Texto estilo={style.titulo} font="malgun">Alterar Senha</Texto>
        </View>

        <InputPrimario
          placeholder="Digite sua senha atual"
          secureTextEntry
          value={alterarSenhaReducer.senha}
          onChangeText={alterarSenhaActions.handleSenha}
          icone="lock-outline"
          lib="material"
        />

        <InputPrimario
          placeholder="Digite sua nova senha"
          secureTextEntry
          value={alterarSenhaReducer.novaSenha}
          onChangeText={alterarSenhaActions.handleNovaSenha}
          icone="lock-outline"
          lib="material"
        />

        <InputPrimario
          placeholder="Confirmar senha"
          secureTextEntry
          value={alterarSenhaReducer.confirmarSenha}
          onChangeText={alterarSenhaActions.handleConfirmarSenha}
          icone="check"
          lib="material"
        />

        <BotaoCircular
          icone="md-checkmark"
          onPress={() => alterarSenhaActions.alterarSenhaUsuario(navigation)}
        />
      </Painel>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer,
    alterarSenhaReducer: state.alterarSenhaReducer
  }
});

const mapDispatchToProps = (dispatch) => ({
  actions: {
    alterarSenhaActions: bindActionCreators(alterarSenhaActions, dispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AlterarSenha);
