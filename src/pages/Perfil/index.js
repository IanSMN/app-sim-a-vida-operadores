import React from 'react'
import { connect } from 'react-redux'

import {
  TextInput,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage
} from 'react-native'

import { bindActionCreators } from 'redux'
import SocketIO from 'socket.io-client'
import Env from '../../environment/desenv'
import * as solicitantesActions from '../../actions/solicitantes'
import * as conversandoActions from '../../actions/conversando'
import { NavigationActions } from 'react-navigation'

import {
  Avatar,
  BarraSuperior,
  Container,
  Icone
} from '../../components'

import { COLOR_DIVISOR_LIGHT } from '../../theme';

import style from './style';

class Perfil extends React.Component {
  constructor(props) {
    super(props)
    this.socket = this.props.navigation.state.params.socket

    this.state = { 
      descricaoTecnica: '',
      status: this.props.navigation.state.params.idStatus 
    }
  }

  componentDidMount() {
    // this.socket.connect()
  }

  selecionarStatus = (status) => {
    this.setState({ ...this.state, status })
  }

  render() {
    const { params } = this.props.navigation.state
    const { sala } = params
    const { nome, imagem } = params.user

    finalizarAtendimento = () => {
      this.socket.emit('finalizarAtendimento', {
        sala: sala,
        idStatus: this.state.status,
        descricaoTecnica: this.state.descricaoTecnica
      });
      AsyncStorage.removeItem('usuario')
      this.props.actions.conversandoActions.handleConversando(false)
      this.props.actions.conversandoActions.handleMensagem([])
      this.props.navigation.dispatch(NavigationActions.back())
      this.props.navigation.dispatch(NavigationActions.back())
      this.props.navigation.dispatch(NavigationActions.back())
    }

    return (
      <Container estilo={{ justifyContent: 'flex-start' }}>
        <BarraSuperior
          buttonLeft={true}
          titulo="Detalhes"
          iconLeft="md-arrow-back"
          onPressButtonLeft={() => this.props.navigation.goBack()}
        />

        {
          imagem
            ?
            <Avatar
              imagem={{ uri: imagem }}
              width={100}
              height={100}
              nome={nome}
              backgroundColor={COLOR_DIVISOR_LIGHT}
              color={'#5F5F5F'}
              top={-10}
              marginTop={110}
              marginBottom={20}
              elevation={0}
            />
            :
            <Avatar
              width={100}
              height={100}
              nome={nome}
              backgroundColor={COLOR_DIVISOR_LIGHT}
              color={'#5F5F5F'}
              top={-10}
              marginTop={110}
              marginBottom={20}
              elevation={0}
            />
        }

        <View style={style.espacoInput}>
          <Icone
            lib='materialCommunity'
            icone="account-outline"
            style={[style.icone, { fontSize: 27 }]}
          />
          <TextInput
            placeholder={nome}
            placeholderTextColor="#757575"
            editable={false}
            style={style.input}
            underlineColorAndroid="transparent"
          />
        </View>

        <View style={style.espacoInput}>
          <Icone
            lib='materialCommunity'
            icone="clipboard-text-outline"
            style={style.icone}
          />
          <TextInput
            placeholder="Descrição"
            value={this.state.descricaoTecnica}
            style={style.input}
            underlineColorAndroid="transparent"
            returnKeyType="next"
            multiline
            autoGrow={true}
            onChangeText={(text) => {
              this.setState({ 
                ...this.state,
                descricaoTecnica: text 
              })
            }}
          />
          <Icone
            icone="pencil"
            lib="materialCommunity"
            style={[style.icone, style.iconeRight]}
          />
        </View>

        <View style={{ marginTop: 15 }}>
          <Text style={style.input}>Alterar Status</Text>

          <View style={{ flexDirection: 'row' }}>
            <View style={{ marginHorizontal: 15 }}>
              <TouchableOpacity onPress={() => this.selecionarStatus(1)}>
                <View style={[style.border, + this.state.status == 1 && style.active]}>
                  <View style={[style.circle, { backgroundColor: '#4caf50' }]}></View>
                </View>
                <Text>Normal</Text>
              </TouchableOpacity>
            </View>

            <View style={{ marginHorizontal: 15 }}>
              <TouchableOpacity onPress={() => this.selecionarStatus(2)}>
                <View style={[style.border, + this.state.status == 2 && style.active]}>
                  <View style={[style.circle, { backgroundColor: '#ffc107' }]}></View>
                </View>
                <Text>Preocupante</Text>
              </TouchableOpacity>
            </View>

            <View style={{ marginHorizontal: 15 }}>
              <TouchableOpacity onPress={() => this.selecionarStatus(3)}>
                <View style={[style.border, + this.state.status == 3 && style.active]}>
                  <View style={[style.circle, { backgroundColor: '#ef5350' }]}></View>
                </View>
                <Text>Grave</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 25 }}>
          <TouchableOpacity 
            onPress={finalizarAtendimento}
            style={style.button}
          >
            <Text style={{ color: '#fff' }}>{'Finalizar conversa'.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    solicitantesReducer: state.solicitantesReducer
  }
});

const mapDispatchToProps = (dispatch) => ({
  actions: {
    solicitantesActions: bindActionCreators(solicitantesActions, dispatch),
    conversandoActions: bindActionCreators(conversandoActions, dispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Perfil);
