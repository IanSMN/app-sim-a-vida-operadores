import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  input: {
    height: 45,
    width: 200,
    fontSize: 16,
    fontFamily: 'corbel'
  },
  textArea: {
    height: 'auto',
    marginBottom: 10,
    fontFamily: 'corbel',
    fontSize: 16,
  },
  espacoInput: {
    width: 260,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  icone: {
    fontSize: 26,
    textAlignVertical: 'center',
    alignSelf: 'center',
    color: '#757575'
  },
  iconeRight: {
    position: 'absolute',
    right: -10,
    fontSize: 20,
    color: '#757575'
  },
  circle: {
    width: 12,
    height: 12,
    padding: 6,
    borderRadius: 100,
    alignSelf: 'center',
  },
  border: {
    zIndex: 99,
    width: 22,
    height: 22,
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#fff',
    justifyContent: 'center',
    borderRadius: 100
  },
  active: {
    borderColor: '#9e9e9e'
  },
  button: {
    height: 40,
    justifyContent: 'center',
    backgroundColor: '#4caf50',
    borderRadius: 3,
    paddingHorizontal: 30,
    elevation: 2
  }
});

export default style;
