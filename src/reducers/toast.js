const INITIAL_STATE = {
  mensagem: '',
  visivel: false
}

import { TOAST_EXIBIR, TOAST_FECHAR } from '../actionTypes/toast'

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOAST_EXIBIR:
      return { ...state, mensagem: action.payload, visivel: true }
    case TOAST_FECHAR:
      return { ...state, visivel: false }
    default:
      return state
  }
}
