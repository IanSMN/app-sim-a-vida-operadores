import {
  SOLICITANTES,
  OPERADORES,
  ATENDENDO,
  GRUPOS
} from '../actionTypes/solicitantes'

const INITIAL_STATE = {
  atendendo: false,
  solicitantes: [],
  operadores: [],
  grupos: []
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SOLICITANTES:
      return { ...state, solicitantes: action.payload }
    case OPERADORES:
      return { ...state, operadores: action.payload }
    case GRUPOS:
      return { ...state, grupos: action.payload }
    case ATENDENDO:
      return { ...state, atendendo: action.payload }
    default:
      return state
  }
}
