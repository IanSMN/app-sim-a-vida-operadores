import { NOTIFICACAO_GRUPO, NOTIFICACAO_OPERADORES, SESSAO } from '../actionTypes/notificacoes'

const INITIAL_STATE = {
  operadores: [],
  grupos: [],
  sessao: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NOTIFICACAO_OPERADORES:
      return { ...state, operadores: action.payload || [] }
    case NOTIFICACAO_GRUPO:
      return { ...state, grupos: action.payload || [] }
    case SESSAO:
      return { ...state, sessao: action.payload }
    default:
      return state
  }
}
