import {
  CONFIRMAR_SENHA,
  NOVA_SENHA,
  SENHA_ATUAL,
  RESET
} from '../actionTypes/alterarSenha'

const INITIAL_STATE = {
  senha: '',
  novaSenha: '',
  confirmarSenha: '',
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SENHA_ATUAL:
      return { ...state, senha: action.payload }
    case NOVA_SENHA:
      return { ...state, novaSenha: action.payload }
    case CONFIRMAR_SENHA:
      return { ...state, confirmarSenha: action.payload }
    case RESET:
      return { ...state, ...INITIAL_STATE }
    default:
      return state
  }
}
