import {
  CONVERSANDO,
  DADOS_MENSAGEM,
  DADOS_MENSAGEM_OLD,
  SALA,
  TIPO,
  CONT
} from '../actionTypes/conversando'

const INITIAL_STATE = {
  conversando: false,
  dadosMensagem: [],
  dadosMensagemOld: [],
  sala: '',
  tipo: '',
  cont: 0
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONVERSANDO:
      return { ...state, conversando: action.payload };
    case DADOS_MENSAGEM:
      return { ...state, dadosMensagem: action.payload };
    case DADOS_MENSAGEM_OLD:
      return { ...state, dadosMensagemOld: action.payload };
    case SALA:
      return { ...state, sala: action.payload };
    case TIPO:
      return { ...state, tipo: action.payload };
    case CONT:
      return { ...state, cont: action.payload };
    default:
      return state;
  }
};
