import {
  GRUPO_LISTA
} from '../actionTypes/pageGrupo'

const INITIAL_STATE = {
  nome: '',
  grupoLista: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GRUPO_LISTA:
      return { ...state, grupoLista: action.payload }
    default:
      return state
  }
}
