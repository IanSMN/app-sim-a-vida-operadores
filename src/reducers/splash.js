import {
  SPLASH
} from '../actionTypes/splash'

const INITIAL_STATE = {
  splash: true
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SPLASH:
      return { ...state, splash: action.payload };
    default:
      return state;
  }
};
