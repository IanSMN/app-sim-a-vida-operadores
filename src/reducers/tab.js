import { INDEX, ROUTES } from '../actionTypes/tab'

const INITIAL_STATE = {
  index: 0,
  routes: [
    { key: '1', title: 'Solicitacões' },
    { key: '2', title: 'Operadores' },
    { key: '3', title: 'Grupos' },
  ]
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case INDEX:
      return { ...state, index: action.payload }
    case ROUTES:
      return { ...state, routes: action.payload }
    default: 
      return state
  }
}
