import { combineReducers } from 'redux'
import pageAutenticacaoSenhaReducer from './pageAutenticacaoSenha'
import alterarSenhaReducer from './alterarSenha'
import usuarioReducer from './usuario'
import solicitantesReducer from './solicitantes'
import toastReducer from './toast'
import conversandoReducer from './conversando'
import splashReducer from './splash'
import notificacoesReducer from './notificacoes'
import tabReducer from './tab'

const reducers = {
  pageAutenticacaoSenhaReducer,
  usuarioReducer,
  solicitantesReducer,
  toastReducer,
  splashReducer,
  conversandoReducer,
  notificacoesReducer,
  alterarSenhaReducer,
  tabReducer
}

export default combineReducers(reducers)
