import {
  PAGE_AUTENTICACAO_SENHA_HANDLE_ALERTA
} from '../actionTypes/pageAutenticacaoSenha'

const INITIAL_STATE = {
  alerta: {
    visivel: false,
    status: 'sucesso',
    mensagem: 'Enviamos um email que permitirá que você redefina sua senha'
  }
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PAGE_AUTENTICACAO_SENHA_HANDLE_ALERTA:
      return { ...state, alerta: { ...state.alerta, ...action.payload } }
    default:
      return state
  }
}
