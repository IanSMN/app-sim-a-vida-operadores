import {
  USUARIO_HANDLE_AUTH,
  USUARIO_HANDLE_DESCRICAO,
  USUARIO_HANDLE_IDADE,
  USUARIO_HANDLE_IMAGEM,
  USUARIO_HANDLE_NOME,
  USUARIO_HANDLE_SENHA,
  USUARIO_HANDLE_EMAIL,
  USUARIO_DATA,
  USUARIO_LOADING,
  TOKEN,
  RESET
} from '../actionTypes/usuario'

const INITIAL_STATE = {
  auth: false,
  descricao: '',
  email: '',
  apelido: '',
  imagem: null,
  nome: '',
  senha: '',
  idade: '',
  token: '',
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USUARIO_HANDLE_AUTH:
      return { ...state, auth: action.payload }
    case USUARIO_HANDLE_DESCRICAO:
      return { ...state, descricao: action.payload }
    case USUARIO_HANDLE_EMAIL:
      return { ...state, email: action.payload }
    case USUARIO_HANDLE_IDADE:
      return { ...state, idade: action.payload }
    case USUARIO_HANDLE_IMAGEM:
      return { ...state, imagem: action.payload }
    case USUARIO_HANDLE_NOME:
      return { ...state, nome: action.payload }
    case USUARIO_HANDLE_SENHA:
      return { ...state, senha: action.payload }
    case TOKEN:
      return { ...state, token: action.payload }
    case USUARIO_DATA:
      return { ...state, ...action.payload }
    case RESET:
      return { ...state, ...INITIAL_STATE }
    case USUARIO_LOADING:
      return { ...state, loading: action.payload }
    default:
      return state
  }
}
