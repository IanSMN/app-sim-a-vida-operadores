import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
  View,
  Text
} from 'react-native'

import Modal from 'react-native-modal'
import style from './style'
import * as toastActions from '../../actions/toast'

const Toast = (props) => {
  const { toastActions } = props.actions
  const { toastReducer } = props.reducers

  return (
    <Modal
      isVisible={toastReducer.visivel}
      onBackButtonPress={toastActions.fechar}
      onBackdropPress={toastActions.fechar}
      style={style.container}
      backdropOpacity={0}
      useNativeDriver
    >
      <View style={style.toast}>
        <Text style={style.texto}>
          {toastReducer.mensagem}
        </Text>
      </View>
    </Modal>
  )
}

const mapStateToProps = (state) => ({
  reducers: {
    toastReducer: state.toastReducer
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    toastActions: bindActionCreators(toastActions, dispatch)
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Toast)
