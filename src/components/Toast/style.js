import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0
  },
  toast: {
    backgroundColor: '#212121',
    justifyContent: 'center',
    width: '100%',
    padding: 10,
    height: 50
  },
  texto: {
    color: '#FFF'
  }
})

export default style;
