import { StyleSheet } from 'react-native'
import { COLOR_TEXT_SECONDARY } from '../../theme'

const style = StyleSheet.create({
  button: {
    justifyContent: 'center',
    minWidth: 125
  },
  text: {
    color: COLOR_TEXT_SECONDARY,
    marginBottom: 25
  },
  touchable: {
    minWidth: 125
  }
});

export default style;
