import React from 'react'

import {
  TouchableOpacity,
  Text,
  View
} from 'react-native'

import style from './style'

export const Botao = ({ onPress, children }) => (
  <TouchableOpacity
    onPress={onPress}
    style={style.touchable}
  >
    <View style={style.button}>
      <Text style={style.text}>
        {children}
      </Text>
    </View>
  </TouchableOpacity>
);
