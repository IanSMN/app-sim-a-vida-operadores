import React from 'react'

import {
  TextInput,
  View
} from 'react-native'

import { Icone } from '../index'
import style from './style'

export const InputPrimario = (props) => {
  return (
    <View style={style.view}    >
      <Icone
        style={style.icone}
        lib={props.lib}
        icone={props.icone}
      />

      <TextInput
        disableFullscreenUI
        underlineColorAndroid="transparent"
        style={style.input}
        placeholder={props.placeholder}
        keyboardType={props.keyboardType}
        value={props.value}
        onChangeText={props.onChangeText}
        autoCapitalize={props.autoCapitalize}
        secureTextEntry={props.secureTextEntry}
        returnKeyType={props.returnKeyType}
        ref={props.referencia}
        onSubmitEditing={props.onSubmitEditing}
      />
    </View>
  )
};
