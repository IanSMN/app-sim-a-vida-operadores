import { StyleSheet } from 'react-native'
import { COLOR_INPUT } from '../../theme'

const style = StyleSheet.create({
  view: {
    alignSelf: 'center',
    width: 300,
    backgroundColor: COLOR_INPUT,
    flexDirection: 'row',
    marginBottom: 15,
    padding: 8
  },
  icone: {
    flex: 1,
    fontSize: 25,
    textAlign: 'center',
    marginTop: 10
  },
  input: {
    height: 45,
    flex: 5
  }
});

export default style;
