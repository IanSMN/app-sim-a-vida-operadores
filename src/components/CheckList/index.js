import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
  FlatList,
  Text,
  View,
  Image,
  TouchableNativeFeedback,
  TextInput
} from 'react-native'

import SocketIO from 'socket.io-client'
import Env from '../../environment/desenv'
import { Icone, BotaoAction } from '../../components'
import style from './style'
import * as toastActions from '../../actions/toast'

export class List extends React.Component {
  constructor(props) {
    super(props)
    this.socket = this.props.navigation.state.params.socket

    this.lista = []

    this.state = {
      text: '',
      lista: [],
      checkedList: []
    }

    for (let i = 0; i < this.props.lista.length; i++) {
      this.lista.push({ ...this.props.lista[i], checked: false })
    }
  }

  componentDidMount() {
    this.setState({ lista: this.lista })
    this.nomeGrupo.focus()
    // console.warn(this.socket)
  }

  checkBox = (index, item) => {
    const novaLista = [...this.state.lista]
    let checkedList = [...this.state.checkedList]
    novaLista[index].checked = !this.state.lista[index].checked

    if (novaLista[index].checked) {
      checkedList.push(novaLista[index])
    } else {
      checkedList.map((value, index) => {
        if (item.nome == value.nome) {
          checkedList.splice(index, 1)
        }
      })
    }

    this.setState({
      lista: [...novaLista],
      checkedList: [...checkedList]
    })
  }

  criarGrupo = () => {
    if (!this.state.text) {
      this.props.actions.toastActions.exibir('Digite o nome do grupo')
      return false
    }
    if (!this.state.checkedList.length) {
      this.props.actions.toastActions.exibir('Selecione um ou mais operadores')
      return false
    }

    const destinatarios = []

    this.state.checkedList.map((value) => {
      destinatarios.push(value.id)
    })

    this.socket.emit('iniciarChat', {
      nome: this.state.text,
      idRemetente: this.props.usuario.id,
      destinatarios: destinatarios,
      tipoConversa: 2
    })

    this.props.navigation.goBack()
    this.props.handleTab(2)
    this.socket.emit('grupos')
    // console.warn('TEM QUE EMITIR GRUPOS, CRIAR GRUPO')
  }

  onChange = (value) => {
    this.setState({ ...this.state, text: value })
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ elevation: 3, backgroundColor: '#fafafa' }}>
          <View style={style.espacoInput}>
            <Icone
              lib="material"
              icone="group-add"
              style={[style.icone, { fontSize: 27 }]}
            />
            <TextInput
              ref={(input) => { this.nomeGrupo = input }}
              onChangeText={this.onChange}
              autoCapitalize="sentences"
              placeholder="Digite o nome do grupo"
              placeholderTextColor="#757575"
              style={style.input}
              underlineColorAndroid="transparent"
            />
          </View>

          <View style={[{ paddingHorizontal: 20 }, this.state.checkedList.length && { borderBottomWidth: 1, borderColor: '#d2d2d2', marginTop: 10 }]}>
            <FlatList
              data={this.state.checkedList}
              horizontal={true}
              renderItem={(item, index) => (
                <View style={{ flexDirection: 'row', height: 40, marginRight: 8, justifyContent: 'center' }}>
                  <Text style={{ fontSize: 16, color: '#616161', }}>{item.item.nome},</Text>
                </View>
              )}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index}
            />
          </View>
        </View>

        <FlatList
          extraData={this.state}
          data={this.state.lista}
          renderItem={
            ({ item, index }) => {
              return (
                <TouchableNativeFeedback onPress={() => this.checkBox(index, item)}>
                  <View style={style.row}>
                    <View style={{ flexDirection: 'row' }}>
                      {item.checked &&
                        <View style={style.checkView}>
                          <Icone style={style.checkIcon} lib="material" icone="check" />
                        </View>
                      }
                      {item.imagem ?
                        <Image style={style.image} source={{ uri: item.imagem }} />
                        :
                        <View style={[style.image, { backgroundColor: item.cor, }]}>
                          <Text style={style.textImage}>{item.nome.charAt() || 'Anônimo'.charAt()}</Text>
                        </View>
                      }
                      <View style={style.fullRow}>
                        <Text style={style.label}>{item.nome || 'Anônimo'}</Text>
                      </View>
                    </View>
                  </View>
                </TouchableNativeFeedback>
              )
            }
          } keyExtractor={(item, index) => index}
        />
        <BotaoAction
          onPress={this.criarGrupo}
          estiloIcon={{ color: '#4caf50' }}
          estilo={{ backgroundColor: '#FFF' }}
          icone="md-checkmark"
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    toastActions: bindActionCreators(toastActions, dispatch)
  }
})

export const CheckList = connect(mapStateToProps, mapDispatchToProps)(List)
