import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  row: {
    paddingLeft: 20,
    paddingRight: 25,
    paddingTop: 30,
  },
  fullRow: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#d2d2d2',
    alignItems: 'center',
  },
  label: {
    fontSize: 18,
    color: '#353535'
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 15,
    marginBottom: 7,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textImage: {
    fontSize: 22,
    color: '#fff'
  },
  checkView: {
    position: 'absolute',
    top: 29,
    left: 30,
    width: 23,
    height: 23,
    zIndex: 999,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4caf50',
    elevation: 2
  },
  checkIcon: {
    fontSize: 14,
    color: '#fff'
  },
  input: {
    height: 45,
    width: '100%',
    fontSize: 18,
    paddingLeft: 10,
    fontFamily: 'corbel'
  },
  espacoInput: {
    flexDirection: 'row',
    marginHorizontal: 22
  },
  icone: {
    fontSize: 26,
    textAlignVertical: 'center',
    alignSelf: 'center',
    color: '#757575'
  },
});

export default style;
