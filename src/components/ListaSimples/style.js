import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginRight: 15,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textImage: {
    fontSize: 22,
    color: '#fff'
  },
  row: {
    marginLeft: 20,
    marginRight: 25,
    paddingTop: 30,
  },
  fullRow: {
    flex: 1,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderColor: '#d2d2d2',
    justifyContent: 'center',
  },
  rightAlign: {
    maxWidth: 85,
    alignItems: 'flex-end'
  },
  label: {
    fontSize: 18,
    color: '#353535'
  },
  subLabel: {
    fontSize: 13,
    marginTop: 3
  },
  status: {
    width: 8,
    height: 8,
    borderRadius: 100,
    marginRight: 6,
    marginTop: 9
  }
})

export default style
