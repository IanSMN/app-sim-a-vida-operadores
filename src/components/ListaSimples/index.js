import React from 'react'

import {
  FlatList,
  Text,
  View,
  Image,
  TouchableWithoutFeedback
} from 'react-native'

import style from './style'

export const ListaSimples = ({ lista, onPressChat, status }) => {
  return (
    <FlatList
      contentContainerStyle={{ marginBottom: 10 }}
      data={lista}
      renderItem={
        ({ item, index }) => {
          const data = new Date(item.dataAbertura)
          const hours = data.getHours().toFixed()
          const minutes = data.getMinutes().toFixed()

          return (
            <View style={style.row}>
              <TouchableWithoutFeedback onPress={() => onPressChat(item, index)}>
                <View style={{ flexDirection: 'row' }}>
                  {item.imagem ?
                    <Image style={style.image} source={{ uri: item.imagem }} />
                    :
                    <View style={[style.image, { backgroundColor: item.cor || '#ffc107', }]}>
                      <Text style={style.textImage}>{item.nome && item.nome.charAt() || ''}</Text>
                    </View>
                  }
                  <View style={style.fullRow}>
                    <Text style={style.label}>{item.nome}</Text>
                    {item.tipoConversa != 2 &&
                      <View style={{ flexDirection: 'row' }}>
                        <View style={[
                          style.status,
                          item.status == true ? { backgroundColor: '#4caf50' } : { backgroundColor: '#f44336' }
                        ]}>
                        </View>
                        <Text style={style.subLabel}>
                          {item.status == true ? 'Online' : 'Offline'}
                        </Text>
                      </View>
                    }
                  </View>
                  {item.notificacoes > 0 &&
                    <View style={[style.fullRow, style.rightAlign]}>
                      <View style={[style.status, { backgroundColor: '#ff5722', width: 20, height: 20, justifyContent: 'center' }]}>
                        <Text style={{ color: '#fff', fontSize: 11, textAlign: 'center' }}>{item.notificacoes}</Text>
                      </View>
                    </View>
                  }
                </View>
              </TouchableWithoutFeedback>
            </View >
          )
        }
      } keyExtractor={(item, index) => index}
    />
  )
}

export default ListaSimples
