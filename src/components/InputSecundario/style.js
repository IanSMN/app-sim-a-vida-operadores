import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  input: {
    alignSelf: 'center',
    height: 50,
    textAlign: 'center',
    width: 200
  }
});

export default style;
