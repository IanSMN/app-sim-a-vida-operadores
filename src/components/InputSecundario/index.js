import React from 'react'
import { TextInput } from 'react-native'
import { COLOR_DIVISOR } from '../../theme'
import style from './style'

export const InputSecundario = (props) => (
  <TextInput
    disableFullscreenUI
    style={style.input}
    placeholder={props.placeholder}
    keyboardType={props.keyboardType}
    value={props.value}
    onChangeText={props.onChangeText}
    underlineColorAndroid={COLOR_DIVISOR}
    secureTextEntry={props.secureTextEntry}
    onSubmitEditing={props.onSubmitEditing}
  />
);
