import React from 'react'

import {
  TouchableOpacity,
  View
} from 'react-native'

import { Icone } from '../index'
import style from './style'

export const BotaoAction = ({ estiloIcon, lib, icone, onPress, estilo }) => (
  <TouchableOpacity
    style={[style.touchable, estilo]}
    onPress={onPress}
  >
    <View
      style={style.button}
    >
      <Icone
        lib={lib}
        icone={icone}
        style={[style.icone, estiloIcon]}
      />
    </View>
  </TouchableOpacity>
);
