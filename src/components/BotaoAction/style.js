import { StyleSheet } from 'react-native'
import { COLOR_ACCENT } from '../../theme'

const style = StyleSheet.create({
  button: {
    height: 56,
    width: 56,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icone: {
    fontSize: 25,
    color: '#fff',
  },
  touchable: {
    borderRadius: 100,
    backgroundColor: COLOR_ACCENT,
    width: 56,
    height: 56,
    elevation: 4,
    position: 'absolute',
    right: 20,
    bottom: 20,
    zIndex: 99
  }
});

export default style;
