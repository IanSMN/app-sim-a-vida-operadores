import { StyleSheet } from 'react-native'
import { DIMENSION_SCREEN_WIDTH } from '../../theme'

const style = StyleSheet.create({
  container: {
    flex: 1,
    width: DIMENSION_SCREEN_WIDTH,
  }
});

export default style;
