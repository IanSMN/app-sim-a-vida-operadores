import React from 'react';

import {
  View,
  Image,
  Text
} from 'react-native';

import { COLOR_LIGHT } from '../../theme';
import style from './style';

export const Avatar = ({ nome = '', width = 85, height = 85, imagem, backgroundColor = '#FCC110', color = COLOR_LIGHT, top = -48, marginTop, marginBottom, elevation = 6 }) => (
  <View style={[style.avatar, { width, height, backgroundColor, top, marginTop, marginBottom, elevation }]}>
    {
      imagem
        ? <Image source={imagem} style={{ borderRadius: 100, width: 100, height: 100 }} resizeMode="cover" />
        : <Text style={[style.letraAvatar, { color }]}>{nome.charAt(0).toUpperCase()}</Text>
    }
  </View>
);
