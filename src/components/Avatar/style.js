import { StyleSheet } from 'react-native'

import {
  COLOR_LIGHT,
  DIMENSION_TEXT_AVATAR
} from '../../theme'

const style = StyleSheet.create({
  avatar: {
    position: 'relative',
    height: 85,
    width: 85,
    alignSelf: 'center',
    borderRadius: 100,
    justifyContent: 'center'
  },
  letraAvatar: {
    color: COLOR_LIGHT,
    fontSize: DIMENSION_TEXT_AVATAR,
    textAlign: 'center',
    textAlignVertical: 'center'
  }
});

export default style;
