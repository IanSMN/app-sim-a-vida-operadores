import React from 'react'

import {
  Text,
  TouchableOpacity,
  View
} from 'react-native'

import Modal from 'react-native-modal'
import style from './style'
import { Icone } from '../index'

export const Alerta = ({ visivel, onDelete, onHide }) => (
  <Modal
    isVisible={visivel}
    useNativeDriver={true}
    onBackdropPress={onHide}
  >
    <View style={style.container}>
      <View style={style.painel}>
        <View style={style.contentText}>
          <View style={style.contentIcon}>
            <Icone style={style.icon} icone="close" lib="material" />
          </View>
          <Text style={style.mensagem}>Você deseja deletar este grupo?</Text>
        </View>
        <View style={style.contentButton}>
          <TouchableOpacity
            style={style.touchable}
            onPress={onHide}
          >
            <Text style={style.textButton}>CANCELAR</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={style.touchable}
            onPress={() => {
              onDelete()
            }}
          >
            <Text style={[style.textButton, { color: '#f44336' }]}>DELETAR</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </Modal>
);
