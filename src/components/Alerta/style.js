import { StyleSheet } from 'react-native'

import {
  COLOR_LIGHT,
  COLOR_PRIMARY
} from '../../theme'

const style = StyleSheet.create({
  container: {
    flex: 1,
    width: 250,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  painel: {
    backgroundColor: COLOR_LIGHT,
    height: 250,
    maxHeight: 300,
    padding: 16,
  },
  contentText: {
    paddingTop: 25,
    height: 190,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mensagem: {
    fontSize: 16,
    textAlign: 'center'
  },
  contentButton:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  touchable: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 90
  },
  textButton: {
    fontSize: 15,
    color: COLOR_PRIMARY
  },
  contentIcon: {
    position: 'absolute',
    top: 15,
    width: 50,
    height: 50,
    borderWidth: 1,
    borderRadius: 100,
    alignItems: 'center',
    borderColor: '#f44336',
    justifyContent: 'center',
  },
  icon: {
    fontSize: 30,
    color: '#f44336'
  }
});

export default style;
