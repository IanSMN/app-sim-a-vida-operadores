import React from 'react'

import {
  TouchableOpacity,
  View
} from 'react-native'

import { Icone } from '../index'
import style from './style'

export const BotaoVoltar = ({ onPress }) => (
  <TouchableOpacity
    style={style.touchable}
    onPress={onPress}
  >
    <View style={style.button}>
      <Icone
        icone="md-arrow-back"
        style={style.icon}
      />
    </View>
  </TouchableOpacity>
);
