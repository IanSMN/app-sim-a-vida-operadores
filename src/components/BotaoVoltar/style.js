import { StyleSheet } from 'react-native'
import { COLOR_ACCENT } from '../../theme'

const style = StyleSheet.create({
  touchable: {
    alignSelf: 'flex-start',
    marginLeft: 16
  },
  button: {
    height: 56,
    width: 56
  },
  icon: {
    color: COLOR_ACCENT,
    fontSize: 30
  }
});

export default style;
