import React from 'react';

import {
  View,
  TouchableOpacity,
  Text,
  Image
} from 'react-native';

import { Icone } from '../../components';
import style from './style';

export const BarraSuperior = props => (
  <View style={[style.container, props.estilo]}>
    {props.buttonLeft === true &&
      <TouchableOpacity style={style.button} onPress={props.onPressButtonLeft}>
          <Icone
            icone={props.iconLeft}
            style={style.icone}
          />
      </TouchableOpacity>
    }

    {props.imagem != null &&
      <Image style={style.image} source={props.imagem} />
    }

    <View>
      <Text style={style.titulo}>
        {props.titulo}
      </Text>
      {props.subtitulo &&
        <Text style={style.subtitulo}>
          {props.subtitulo}
        </Text>
      }
    </View>

    {props.buttonRightBefore === true &&
      <View style={{ flex: 4, alignItems: 'flex-end' }}>
        <View style={props.notificacao && style.notificacao}></View>
        <TouchableOpacity style={style.button} onPress={props.onPressbuttonRightBefore}>
          <View>
            <Icone
              lib="material"
              icone={props.iconRightBefore}
              style={style.icone}
            />
          </View>
        </TouchableOpacity>
      </View>
    }

    {props.buttonRight === true &&
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <TouchableOpacity style={style.button} onPress={props.onPressButtonRight}>
          <View>
            <Icone
              lib="material"
              icone={props.iconRight}
              style={style.icone}
            />
          </View>
        </TouchableOpacity>
      </View>
    }
  </View>
);
