import { StyleSheet } from 'react-native'
import { COLOR_LIGHT } from '../../theme'

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 75,
    backgroundColor: '#616161',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 99,
    elevation: 5,
    paddingVertical: 16,
    paddingLeft: 0,
    paddingRight: 0
  },
  image:{
    width: 50,
    height: 50,
    borderRadius: 100,
    marginTop: 3,
    marginHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icone: {
    color: COLOR_LIGHT,
    fontSize: 25
  },
  textImage: {
    fontSize: 22,
    color: '#fff'
  },
  titulo: {
    fontSize: 19,
    marginLeft: 10,
    color: COLOR_LIGHT,
  },
  subtitulo: {
    fontSize: 12,
    color: COLOR_LIGHT,
  },
  button: {
    width: 50,
    height: 75,
    alignItems: 'center',
    justifyContent: 'center'
  },
  notificacao: {
    top: 20,
    right: 10,
    width: 8,
    height: 8,
    borderRadius: 100,
    backgroundColor: '#fafafa',
    position: 'absolute'
  }
});

export default style;
