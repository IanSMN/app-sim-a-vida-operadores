import { StyleSheet } from 'react-native'
import { COLOR_ACCENT, COLOR_LIGHT } from '../../theme'

const style = StyleSheet.create({
  button: {
    height: 76,
    width: 76,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icone: {
    fontSize: 38,
    color: COLOR_ACCENT,
  },
  touchable: {
    borderRadius: 100,
    borderWidth: 1,
    borderColor: COLOR_ACCENT,
    backgroundColor: COLOR_LIGHT,
    alignSelf: 'center',
    width: 76,
    height: 76
  }
});

export default style;
