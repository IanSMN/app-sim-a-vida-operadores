import React from 'react'

import {
  TouchableOpacity,
  View
} from 'react-native'

import { Icone } from '../index'
import style from './style'

export const BotaoCircular = ({ icone, onPress, estilo }) => (
  <TouchableOpacity
    style={[style.touchable, estilo]}
    onPress={onPress}
  >
    <View
      style={style.button}
    >
      <Icone
        icone={icone}
        style={style.icone}
      />
    </View>
  </TouchableOpacity >
);
