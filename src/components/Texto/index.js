import React from 'react'
import { Text } from 'react-native'
import style from './style'

export const Texto = ({ children, estilo, font }) => {
  let texto;
  switch (font) {
    case 'roboto':
      texto = <Text style={[style.roboto, estilo]}>{children}</Text>
      break;
    case 'classic':
      texto = <Text style={[style.classic, estilo]}>{children}</Text>
      break;
    case 'corbel':
      texto = <Text style={[style.corbel, estilo]}>{children}</Text>
      break;
    case 'malgun':
      texto = <Text style={[style.malgun, estilo]}>{children}</Text>
      break;
    case 'segoe':
      texto = <Text style={[style.segoe, estilo]}>{children}</Text>
      break;
    default:
      texto = <Text style={estilo}>{children}</Text>
      break;
  }
  return texto;
};
