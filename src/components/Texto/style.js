import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  roboto: {
    fontFamily: 'Roboto'
  },
  classic: {
    fontFamily: 'PerfumeClassic'
  },
  corbel: {
    fontFamily: 'corbel'
  },
  malgun: {
    fontFamily: 'malgun'
  },
  segoe: {
    fontFamily: 'SEGOEUI'
  }
  
});

export default style;
