import React from 'react'

import {
  View,
  Image
} from 'react-native'

import style from './style'

export const Media = ({ source }) => (
  <View>
    <Image
      source={source}
      style={style.imagem}
    />
  </View>
);
