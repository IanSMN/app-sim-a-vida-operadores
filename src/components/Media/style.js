import { StyleSheet } from 'react-native'
import { DIMENSION_SCREEN_WIDTH } from '../../theme'

const style = StyleSheet.create({
  imagem: {
    height: 300,
    width: DIMENSION_SCREEN_WIDTH,
    resizeMode: 'cover'
  }
});

export default style;
