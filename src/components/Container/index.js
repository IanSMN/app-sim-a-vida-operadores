import React from 'react'

import {
  ScrollView,
  StatusBar
} from 'react-native'

import { COLOR_PRIMARY_DARK } from '../../theme'
import style from './style'

export const Container = ({ hiddenStatusBar, children, estilo }) => (
  <ScrollView
    contentContainerStyle={[style.container, estilo]}
  >
    <StatusBar
      hidden={hiddenStatusBar}
      backgroundColor={COLOR_PRIMARY_DARK}
    />
    {children}
  </ScrollView>
);
