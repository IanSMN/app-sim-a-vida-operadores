import { StyleSheet } from 'react-native'

import {
  COLOR_LIGHT
} from '../../theme'

const style = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR_LIGHT
  }
});

export default style;
