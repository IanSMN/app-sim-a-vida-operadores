import React from 'react'
import IconIon from 'react-native-vector-icons/Ionicons'
import IconFont from 'react-native-vector-icons/FontAwesome'
import IconOcticons from 'react-native-vector-icons/Octicons'
import IconEntypo from 'react-native-vector-icons/Entypo'
import Feather from 'react-native-vector-icons/Feather'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export const Icone = ({ lib, icone, style }) => {
  let Icone;
  switch (lib) {
    case 'font':
      Icone = <IconFont name={icone} style={style} />;
      break;
    case 'oct':
      Icone = <IconOcticons name={icone} style={style} />;
      break;
    case 'ent':
      Icone = <IconEntypo name={icone} style={style} />;
      break;
    case 'feat':
      Icone = <Feather name={icone} style={style} />;
      break;
    case 'evil':
      Icone = <EvilIcons name={icone} style={style} />;
      break;
    case 'material':
      Icone = <MaterialIcons name={icone} style={style} />;
      break;
    case 'materialCommunity':
      Icone = <MaterialCommunityIcons name={icone} style={style} />;
      break;
    default:
      Icone = <IconIon name={icone} style={style} />;
  }
  return Icone;
};
