import { StyleSheet } from 'react-native'

import {
  COLOR_PRIMARY,
  COLOR_LIGHT
} from '../../theme'

const style = StyleSheet.create({
  titulo: {
    marginTop: 7,
    textAlign: 'center',
    color: '#353535',
    fontSize: 16
  },
  container: {
    flex: 1,
    width: 250,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  painel: {
    backgroundColor: COLOR_LIGHT,
    height: 235,
    maxHeight: 300,
    padding: 16,
  },
  contentText: {
    height: 150,
    alignItems: 'center',
  },
  contentButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  touchable: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 90
  },
  textButton: {
    fontSize: 15,
    color: COLOR_PRIMARY
  }
});

export default style;
