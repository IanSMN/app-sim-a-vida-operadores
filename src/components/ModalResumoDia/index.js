import React from 'react'

import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'

import Modal from 'react-native-modal'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import style from './style'
import * as usuarioActions from '../../actions/usuario'

const ResumoDia = ({ visivel, onPress, onHide, reducers, actions }) => {
  const { usuarioReducer } = reducers
  const { usuarioActions } = actions
  return (
    <Modal
      isVisible={visivel}
      useNativeDriver={true}
      onBackdropPress={onHide}
    >
      <View style={style.container}>
        <View style={style.painel}>
          <Text style={style.titulo}>Resumo do dia</Text>
          <View style={style.contentText}>
            <TextInput
              value={usuarioReducer.descricao}
              onChangeText={usuarioActions.handleDescricao}
              style={{ width: 200, minHeight: 100 }}
              autoGrow={true}
              multiline={true}
              returnKeyType="done"
              maxLength={100}
              placeholder="Observação"
            />
          </View>
          <View
            style={style.contentButton}>
            <TouchableOpacity
              onPress={onHide}
              style={style.touchable}
            >
              <Text style={style.textButton}>CANCELAR</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={onPress}
              style={style.touchable}
            >
              <Text style={[style.textButton]}>FINALIZAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  )
}

const mapStateToProps = (state) => ({
  reducers: {
    usuarioReducer: state.usuarioReducer,
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    usuarioActions: bindActionCreators(usuarioActions, dispatch),
  }
})

export const ModalResumoDia = connect(mapStateToProps, mapDispatchToProps)(ResumoDia)

