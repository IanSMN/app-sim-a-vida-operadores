import React from 'react'

import {
  FlatList,
  Text,
  View,
  Image,
  TouchableWithoutFeedback
} from 'react-native'

import style from './style'
// import console = require('console');

export const Lista = ({ lista, onPressChat, status }) => {
  return (
    <FlatList
      data={lista}
      renderItem={
        ({ item, index }) => {
          const data = item.dataAbertura
          const time = data.substring(11, 16)

          const setSubLabel = (status) => {
            switch (status) {
              case 1:
                return 'Estado normal'
              case 2:
                return 'Estado preocupante'
              case 3:
                return 'Estado grave'
              default:
                return 'Sem descrição'
            }
          }

          return (
            <View style={style.row}>
              {/* <Text>{JSON.stringify(item)}</Text> */}
              <TouchableWithoutFeedback onPress={() => onPressChat(item, index)}>
                <View style={{ flexDirection: 'row' }}>
                  {item && item.user && item.user.imagem ?
                    <Image style={style.image} source={{ uri: item.user.imagem || null }} />
                    :
                    <View style={[style.image, { backgroundColor: '#ffc107', }]}>
                      <Text style={style.textImage}>{item.user.nome.charAt()}</Text>
                    </View>
                  }
                  <View style={style.fullRow}>
                    <Text style={style.label}>{item.user.nome}</Text>
                    {status &&
                      <View style={{ flexDirection: 'row' }}>
                        <View style={[
                          style.status,
                          (item.idStatus == null) ? { backgroundColor: '#cccbcb' } :
                            ((item.idStatus == 1) ? { backgroundColor: '#4caf50' } :
                              ((item.idStatus == 2)) ? { backgroundColor: '#ffc107' } : { backgroundColor: '#ef5350' })
                        ]}>
                        </View>
                        <Text style={style.subLabel}>
                          {setSubLabel(item.idStatus)}
                        </Text>
                      </View>
                    }
                  </View>
                  <View style={[style.fullRow, style.rightAlign]}>
                    <Text style={style.subLabel}>{time || '0:00'}</Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          )
        }
      } keyExtractor={(item, index) => index}
    />

  )
}

export default Lista;
