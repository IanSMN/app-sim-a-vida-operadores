import React from 'react'
import { View } from 'react-native'
import { TabBar as TabLib } from 'react-native-tab-view'
import { BarraSuperior } from '../BarraSuperior'
import style from './style'

export const TabBar = ({ titulo, onPressButton, estilo, ...props }) => (
  <View style={{ marginBottom: 20 }}>
    <BarraSuperior
      buttonRight={true}
      iconRight="exit-to-app"
      estilo={{ elevation: 0 }}
      titulo={titulo}
      onPressButtonRight={onPressButton}
    />
    <TabLib
      {...props}
      style={[style.tab, estilo]}
      labelStyle={style.label}
      indicatorStyle={{
        height: 3,
        backgroundColor: '#FCC110'
      }}
    />
  </View>
);
