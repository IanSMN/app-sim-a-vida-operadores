import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  tab: {
    transform: [{
      translateY: 62
    }],
    marginBottom: 35,
    zIndex: 9999,
    elevation: 0,
    backgroundColor: '#616161'
  },
  label: {
    margin: 0,
    textAlign: 'center',
    fontFamily: 'Roboto',
    paddingVertical: 12,
    fontWeight: 'bold'
  },
  indicator: {
    backgroundColor: '#ffca28'
  }
});

export default style;
