import { StyleSheet } from 'react-native'
import { DIMENSION_SCREEN_WIDTH } from '../../theme'

const style = StyleSheet.create({
  container: {
    height: 450,
    width: DIMENSION_SCREEN_WIDTH,
    justifyContent: 'space-around'
  }
});

export default style;
