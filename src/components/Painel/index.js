import React from 'react'
import { View } from 'react-native'
import style from './style'

export const Painel = ({ children }) => (
  <View style={style.container}>
    {children}
  </View >
);
