import React from 'react'

import { bindActionCreators } from 'redux'

import { connect } from 'react-redux'

import {
  View,
  NetInfo,
  AppState,
  Alert,
  Modal,
  StyleSheet,
  AsyncStorage,
  Text
} from 'react-native'
import StackAutenticacao from '../nav/StackAutenticacao'
import * as usuarioActions from '../actions/usuario'
import StackNavigate from '../nav/StackNavigate'
import Toast from '../components/Toast'
import SocketIO from 'socket.io-client'
import Env from '../environment/desenv'
import { Splash } from '../components'

class Inicial extends React.Component {
  constructor(props) {
    super(props)
    this.socket = SocketIO(
      Env.SOCKET_API, {
        transports: ['websocket'],
        timeout: 10000,
        reconnection: false
      })
    this.state = {
      loading: false
    }
  }

  componentDidMount() {
    const { usuarioActions } = this.props.actions
    NetInfo.isConnected.addEventListener('connectionChange', this.onChangeNetInfo)
    AppState.addEventListener('change', this.appStateListener)
    usuarioActions.refazer()

    this.socket.on('refazerLogin', () => {
      this.appStateListener('active')
    })

    this.socket.on('conexaoDuplicada', () => {
      // console.warn('CONEXAO DUPLICADA')
      Alert.alert(
        'Atenção!',
        'O Sim a vida já esta com uma sessão aberta em outro dispositivo',
        // [{ text: 'Fechar', onPress: RNExitApp.exitApp }],
        // [{ text: 'Fechar', onPress: this.socket.disconnect() }],
        [{ text: 'Fechar', onPress: console.log('Close') }],
        { cancelable: false }
      )
      this.props.actions.usuarioActions.deslogar()
      this.socket.disconnect()
    })

    this.socket.on('connect_error', error => {
      this.appStateListener('active')
    })

    this.socket.on('reconnect_attempt', (rec) => {
      this.socket.io.opts.transports = ['websocket']
    })

    this.socket.on('disconnect', (reason) => {
      this.appStateListener('active')
    })

    this.socket.on('autenticacao', async value => {
      await AsyncStorage.setItem('Autenticou', 'true')
      this.setState({ loading: false })
    })

    this.socket.on('autenticar', () => {
      this.autentication(this.props.reducers.usuarioReducer)
    })
  }

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.onChangeNetInfo)
    AppState.removeEventListener('change', this.appStateListener)
  }

  appStateListener = async (state) => {
    const autenticado = await AsyncStorage.getItem('Autenticou')
    const usuario = await AsyncStorage.getItem('usuario')

    if (state === 'active') {
      if (this.props.reducers.usuarioReducer.auth) {
        if (!autenticado) {
          // console.warn(autenticado)
          this.loading(true)
          // this.socket.connect()
          setTimeout(() => {
            this.autentication(this.props.reducers.usuarioReducer)
          }, 2000)
        } else {
          // this.socket.disconnect()
          this.loading(false)
        }
      } else {
        this.socket.disconnect()
      }
    } else if (state === 'background') {
      // if (!usuario) {
      //   this.socket.disconnect()
      //   AsyncStorage.removeItem('Autenticou')
      // }
    } else if (state === 'inactive') {
      console.log('TA FECHADO BIXO')
    }
  }

  onChangeNetInfo = (conectado) => {
    if (!conectado) {
      this.props.actions.usuarioActions.deslogar()
      this.props.actions.usuarioActions.semConexao()
    }
  }

  loading = (value) => {
    this.setState({ loading: value })
  }

  renderInitial() {
    const { usuarioReducer, splashReducer } = this.props.reducers
    if (splashReducer.splash) return <Splash />
    if (usuarioReducer.auth) {
      this.autentication(this.props.reducers.usuarioReducer)
      return <StackNavigate screenProps={{ socket: this.socket }} />
    } else {
      this.socket.disconnect()
      return <StackAutenticacao />
    }
  }

  autentication = async (usuarioReducer) => {
    const token = await usuarioReducer.token
    const autenticado = await AsyncStorage.getItem('Autenticou')
    this.socket.connect()
    if (!autenticado && token) {
      // console.warn(token)
      this.socket.emit('autenticacao', {
        token: token,
        tipo: 'operador'
      })
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderInitial()}
        <Toast />
        <Modal
          animationType={'fade'}
          transparent={true}
          visible={this.state.loading}
          onRequestClose={() => this.loading(false)}
        >
          <View style={style.wrapper}>
            <Text style={style.text}>Carregando...</Text>
          </View>
        </Modal>
      </View>
    )
  }
}

const style = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 9
  },
  text: {
    color: '#fff',
    fontSize: 25
  }
})

const mapStateToProps = (state) => ({
  reducers: {
    conversandoReducer: state.conversandoReducer,
    usuarioReducer: state.usuarioReducer,
    splashReducer: state.splashReducer,
  }
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    usuarioActions: bindActionCreators(usuarioActions, dispatch)
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Inicial)
