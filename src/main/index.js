import React from 'react'
import { Provider } from 'react-redux'
import Inicial from './Inicial'
import store from './store'

const Main = () => (
  <Provider store={store}>
    <Inicial />
  </Provider>
)

export default Main;
