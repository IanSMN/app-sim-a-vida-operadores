import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

import reducers from '../reducers'

const enhancers = composeEnhancers(
  applyMiddleware(thunk)
);

const store = createStore(reducers, enhancers);

export default store;
