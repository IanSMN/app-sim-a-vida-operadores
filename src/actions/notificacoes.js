import { NOTIFICACAO_GRUPO, NOTIFICACAO_OPERADORES, SESSAO } from '../actionTypes/notificacoes'

import * as solicitantesActions from './solicitantes'

export const notificarOperadores = (notificacao) => {
  return (dispatch) => {
    dispatch(handleOperadores(notificacao))
  }
}

export const notificarGrupos = (notificacao) => {
  return (dispatch) => {
    dispatch(handleGrupos(notificacao))
  }
}

export const removerNotificacoes = (i, tipo, id) => {
  return (dispatch, getState) => {
    const { solicitantesReducer, notificacoesReducer } = getState()
    const tipoSolicitante = solicitantesReducer[tipo]
    const notificantes = notificacoesReducer[tipo]

    notificantes.map((item, index) => {
      if (item[id] === tipoSolicitante[i].id) {
        tipoSolicitante[i].notificacoes = 0
        notificantes.splice(index, 1)
      }
    })

    if(tipo === 'operadores') {
      dispatch(notificarOperadores(notificantes))
      dispatch(solicitantesActions.listarOperadores(tipoSolicitante))
    } else {
      dispatch(notificarGrupos(notificantes))
      dispatch(solicitantesActions.listarGrupos(tipoSolicitante))
    }
  }
}

export const handleGrupos = (notificacao) => ({
  type: NOTIFICACAO_GRUPO,
  payload: notificacao
})

export const handleOperadores = (notificacao) => ({
  type: NOTIFICACAO_OPERADORES,
  payload: notificacao
})

export const handleSessao = (sessao) => {
  return {
    type: SESSAO,
    payload: sessao
  }
}
