import {
  PAGE_AUTENTICACAO_SENHA_HANDLE_ALERTA
} from '../actionTypes/pageAutenticacaoSenha'

export const recuperarSenha = () => {
  return (dispatch) => {
    dispatch(handleAlerta({ visivel: true }));
  };
};

export const hideAlerta = () => {
  return (dispatch) => {
    dispatch(handleAlerta({ visivel: false }));
  };
};

export const handleAlerta = (alerta) => {
  return {
    type: PAGE_AUTENTICACAO_SENHA_HANDLE_ALERTA,
    payload: alerta
  };
};
