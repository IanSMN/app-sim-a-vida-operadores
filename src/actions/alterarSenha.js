import {
  NOVA_SENHA,
  SENHA_ATUAL,
  CONFIRMAR_SENHA,
  RESET
} from '../actionTypes/alterarSenha'
import { alterarSenha } from '../service/usuario'
import { NavigationActions } from 'react-navigation'
import * as toastActions from './toast'

export const alterarSenhaUsuario = (navigation) => async (dispatch, getState) => {
  const { alterarSenhaReducer, usuarioReducer } = getState()

  try {
    await alterarSenha({
      login: usuarioReducer.nome,
      senha: alterarSenhaReducer.senha,
      novaSenha: alterarSenhaReducer.novaSenha,
      confirmacaoNovaSenha: alterarSenhaReducer.confirmarSenha,
    })

    navigation.dispatch(NavigationActions.back())
    navigation.dispatch(NavigationActions.back())
    dispatch(toastActions.exibir('Senha alterada com sucesso'))
    dispatch(handleReset())

  } catch (error) {
    dispatch(toastActions.exibir(error.response.data.message || 'Erro no servidor'))
  }
}


export const handleReset = () => {
  return {
    type: RESET,
    payload: null
  };
};

export const handleSenha = (senha) => {
  return {
    type: SENHA_ATUAL,
    payload: senha
  };
};

export const handleConfirmarSenha = (senha) => {
  return {
    type: CONFIRMAR_SENHA,
    payload: senha
  };
};

export const handleNovaSenha = (senha) => {
  return {
    type: NOVA_SENHA,
    payload: senha
  };
};
