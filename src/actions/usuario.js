import {
  USUARIO_HANDLE_AUTH,
  USUARIO_HANDLE_DESCRICAO,
  USUARIO_HANDLE_IMAGEM,
  USUARIO_HANDLE_NOME,
  USUARIO_HANDLE_SENHA,
  USUARIO_HANDLE_EMAIL,
  USUARIO_LOADING,
  USUARIO_DATA,
  TOKEN,
  RESET
} from '../actionTypes/usuario'

import RNExitApp from 'react-native-exit-app'

import * as toastActions from './toast'
import { SPLASH } from '../actionTypes/splash'
import { AsyncStorage, NetInfo, Alert } from 'react-native'
import { autenticarEmail, autenticarLogin, refazLogin } from '../service'

export const autenticarUsuario = (usuario, navigation) => async (dispatch) => {
  try {
    dispatch(handleLoading(true))
    await autenticarEmail({ login: usuario })
    navigation.navigate({key: 'authSenha', routeName: 'AutenticacaoSenha' })
  } catch (error) {
    console.log(error)
    if (!error.response.data) {
      Alert.alert(
        'Atenção!',
        'Por favor verifique sua conexão ou tente novamente mais tarde',
        [{ text: 'Ok', onPress: RNExitApp.exitApp }],
        { cancelable: false }
      )
    } else {
      dispatch(toastActions.exibir(error.response.data.message))
    }
  } finally {
    dispatch(handleLoading(false))
  }
}

export const autenticar = (navigation) => async (dispatch, getState) => {
  const { usuarioReducer } = getState()
  try {
    dispatch(handleLoading(true))
    const res = await autenticarLogin({
      login: usuarioReducer.nome,
      senha: usuarioReducer.senha,
    })
    const { user, token } = res.data.content

    dispatch(handleAuth(true))
    dispatch(handleToken(token))
    dispatch(handleUsuario(user))
    await AsyncStorage.setItem('@Token:key', token)
  }
  catch (error) {
    if (error.response.data.executionCode === 4) {
      navigation.navigate({key: 'alterarSenha', routeName: 'AlterarSenha' })
    }
    if (!error.response.data) {
      Alert.alert(
        'Atenção!',
        'Por favor verifique sua conexão ou tente novamente mais tarde',
        [{ text: 'Ok', onPress: RNExitApp.exitApp }],
        { cancelable: false }
      )
    } else {
      dispatch(toastActions.exibir(error.response.data.message))
    }
  } finally {
    dispatch(handleLoading(false))
  }
}

export const refazer = () => async (dispatch, getState) => {
  const { usuarioReducer } = getState()
  dispatch(handleSplash(true))

  let connection

  try {
    connection = await NetInfo.getConnectionInfo()
  }
  catch (error) {
    dispatch(toastActions.exibir(error))
    dispatch(handleSplash(false))
  }

  if (connection.type !== 'none') {
    try {
      const token = await AsyncStorage.getItem('@Token:key')
      if (token) {
        const res = await refazLogin({ 'Authentication': token })
        dispatch(handleAuth(true))
        dispatch(handleToken(token))
        dispatch(handleUsuario(res.data.content.user))
      }
    } catch (error) {
      if (!error.response) {
        Alert.alert(
          'Atenção!',
          'Por favor verifique sua conexão ou tente novamente mais tarde',
          [{ text: 'Ok', onPress: RNExitApp.exitApp }],
          { cancelable: false }
        )
      } else {
        dispatch(toastActions.exibir(error.response.data.message))
      }
    }
  }
  else {
    dispatch(toastActions.exibir('Sem conexão com internet'))
  }
  dispatch(handleSplash(false))
}

export const semConexao = () => (dispatch) => {
  dispatch(toastActions.exibir('Sem conexão com internet'))
}

export const handleLoading = (loading) => {
  return {
    type: USUARIO_LOADING,
    payload: loading
  }
}

export const handleReset = () => {
  return {
    type: RESET,
    payload: null
  }
}

export const handleUsuario = (usuario) => {
  return {
    type: USUARIO_DATA,
    payload: usuario
  }
}

export const handleSplash = (splash) => {
  return {
    type: SPLASH,
    payload: splash
  }
}

export const handleToken = (token) => {
  return {
    type: TOKEN,
    payload: token
  }
}

export const deslogar = () => {
  return (dispatch) => {
    dispatch(handleReset())
    AsyncStorage.clear()
  }
}

export const handleAuth = (auth) => {
  return {
    type: USUARIO_HANDLE_AUTH,
    payload: auth
  };
};

export const handleDescricao = (descricao) => {
  return {
    type: USUARIO_HANDLE_DESCRICAO,
    payload: descricao
  };
};

export const handleImagem = (imagem) => {
  return {
    type: USUARIO_HANDLE_IMAGEM,
    payload: imagem
  }
};

export const handleEmail = (email) => {
  return {
    type: USUARIO_HANDLE_EMAIL,
    payload: email
  };
};

export const handleSenha = (senha) => {
  return {
    type: USUARIO_HANDLE_SENHA,
    payload: senha
  };
};

export const handleNome = (nome) => {
  return {
    type: USUARIO_HANDLE_NOME,
    payload: nome
  };
};
