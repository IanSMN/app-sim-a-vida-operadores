import {
  CONVERSANDO,
  DADOS_MENSAGEM,
  DADOS_MENSAGEM_OLD,
  SALA,
  TIPO,
  CONT
} from '../actionTypes/conversando'

import { GiftedChat } from 'react-native-gifted-chat'

export const handleConversando = (conversa) => {
  return {
    type: CONVERSANDO,
    payload: conversa
  };
};

export const renderMessage = (mensagem, paginacao) => {
  return (dispatch, getState) => {
    const { conversandoReducer } = getState()
    const mensagens = conversandoReducer.dadosMensagem
    const novaMensagem = !paginacao ? GiftedChat.append(mensagens, mensagem) : GiftedChat.prepend(mensagens, mensagem)
    dispatch(handleMensagem(novaMensagem))
  }
}

export const handleMensagem = (mensagem) => {
  return {
    type: DADOS_MENSAGEM,
    payload: mensagem
  }
}

export const handleMensagemOld = (mensagem) => {
  return {
    type: DADOS_MENSAGEM_OLD,
    payload: mensagem
  }
}


export const handleSala = (sala) => {
  return {
    type: SALA,
    payload: sala
  }
}

export const handleTipo = (tipo) => {
  return {
    type: TIPO,
    payload: tipo
  }
}

export const handleCont = (cont) => {
  return {
    type: CONT,
    payload: cont
  }
}
