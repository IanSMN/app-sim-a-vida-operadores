import { TOAST_EXIBIR, TOAST_FECHAR } from '../actionTypes/toast'

export const exibir = (mensagem) => ({
  type: TOAST_EXIBIR,
  payload: mensagem
})

export const fechar = () => ({
  type: TOAST_FECHAR,
  payload: null
})
