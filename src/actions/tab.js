import { INDEX } from '../actionTypes/tab'

export const handleTab = (index) => ({
  type: INDEX,
  payload: index
})
