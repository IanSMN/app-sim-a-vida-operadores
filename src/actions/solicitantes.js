import {
  SOLICITANTES,
  ATENDENDO,
  OPERADORES,
  GRUPOS
} from '../actionTypes/solicitantes'

export const listarSolicitantes = (usuarios) => {
  return (dispatch, getState) => {
    const { solicitantesReducer } = getState()
    const solicitantesAtualizados = [...usuarios]

    dispatch(handleSolicitantes(solicitantesAtualizados))
  }
}

export const listarOperadores = (usuarios = []) => {
  return (dispatch, getState) => {
    const { solicitantesReducer, usuarioReducer } = getState()
    const { operadores } = solicitantesReducer
    const operadoresAtualizados = usuarios.filter((usuario) => usuario.id !== usuarioReducer.id)

    dispatch(handleOperadores(operadoresAtualizados))
  }
}

export const listarGrupos = (grupo) => {
  return (dispatch, getState) => {
    const { solicitantesReducer } = getState()
    const { grupos } = solicitantesReducer
    const gruposAtualizados = [...grupo]

    dispatch(handleGrupos(gruposAtualizados))
  }
}

export const handleSolicitantes = (solicitante) => {
  return {
    type: SOLICITANTES,
    payload: solicitante
  }
}

export const handleOperadores = (operadores) => {
  return {
    type: OPERADORES,
    payload: operadores
  }
}


export const handleAtendendo = (atendendo) => {
  return {
    type: ATENDENDO,
    payload: atendendo
  }
}

export const handleGrupos = (grupos) => {
  return {
    type: GRUPOS,
    payload: grupos
  }
}
