import Env from '../environment/desenv'
import axios from 'axios'

const headers = { 'Content-Type': 'application/json', 'System': Env.SMN_AUTH_ID }

export const autenticarEmail = (usuario) => {
  return axios.post(`${Env.SMN_AUTH_API}/login/dados`, usuario, { headers })
}

export const autenticarLogin = (usuario) => {
  return axios.post(`${Env.SMN_AUTH_API}/login`, usuario, { headers })
}

export const refazLogin = (token) => {
  return axios.get(`${Env.SMN_AUTH_API}/login/refazer`, { headers: { ...headers, ...token }})
}

export const alterarSenha = (usuario) => {
  return axios.post(`${Env.SMN_AUTH_API}/login/alterar-senha`, usuario, { headers })
}
