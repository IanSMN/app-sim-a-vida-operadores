import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  image: {
    width: 38,
    height: 38
  }
});

export default style;
