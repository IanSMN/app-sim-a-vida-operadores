import { StackNavigator } from 'react-navigation'
import StackPerfil from '../StackPerfil'
import StackChat from '../StackChat'
import Chat from '../../pages/Chat'
import NovoGrupo from '../../pages/NovoGrupo'

const StackNavigate = StackNavigator(
  {
    StackChat: {
      screen: StackChat
    },
    StackPerfil: {
      screen: StackPerfil
    },
    Chat: {
      screen: Chat
    },
    NovoGrupo: {
      screen: NovoGrupo
    }
  },
  {
    headerMode: "none"
  }
);

export default StackNavigate;
