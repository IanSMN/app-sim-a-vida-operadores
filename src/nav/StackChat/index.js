import { StackNavigator } from 'react-navigation'
import Chat from '../../pages/Chat'
import Usuarios from '../../pages/Usuarios'
import Perfil from '../../pages/Perfil'
import Operadores from '../../pages/Usuarios/Operadores'


const StackChat = StackNavigator(
  {
    Usuarios: {
      screen: Usuarios
    },
    Chat: {
      screen: Chat
    },
    Perfil: {
      screen: Perfil
    },
    Operadores: {
      screen: Operadores
    }
  },
  {
    headerMode: 'none'
  });

export default StackChat;
