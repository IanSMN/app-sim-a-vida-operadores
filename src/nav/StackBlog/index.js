import { StackNavigator } from 'react-navigation';
import Artigo from '../../pages/Artigo';
import Blog from '../../pages/Blog';

const StackBlog = StackNavigator(
  {
    Blog: {
      screen: Blog
    },
    Artigo: {
      screen: Artigo
    }
  },
  {
    headerMode: 'none'
  });

export default StackBlog;
