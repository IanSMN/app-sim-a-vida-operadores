import { StackNavigator } from 'react-navigation'
import AutenticacaoEmail from '../../pages/AutenticacaoEmail'
import AutenticacaoSenha from '../../pages/AutenticacaoSenha'
import AlterarSenha from '../../pages/AlterarSenha'

const StackAutenticacao = StackNavigator(
  {
    AutenticacaoEmail: {
      screen: AutenticacaoEmail
    },
    AutenticacaoSenha: {
      screen: AutenticacaoSenha
    },
    AlterarSenha: {
      screen: AlterarSenha
    }
  },
  {
    headerMode: 'none'
  }
);

export default StackAutenticacao;
