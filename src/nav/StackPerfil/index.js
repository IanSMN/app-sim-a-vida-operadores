import { StackNavigator } from 'react-navigation'
import Perfil from '../../pages/Perfil'

const StackPerfil = StackNavigator(
  {
    Perfil: {
      screen: Perfil
    }
  },
  {
    headerMode: "none"
  });

export default StackPerfil;
